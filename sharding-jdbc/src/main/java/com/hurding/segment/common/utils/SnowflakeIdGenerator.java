package com.hurding.segment.common.utils;

import java.util.concurrent.atomic.AtomicLong;

public class SnowflakeIdGenerator {
    // 起始的时间戳，2020-01-01 00:00:00
    private final long START_TIMESTAMP = 1577808000000L;

    // 每部分占用的位数
    private final long SEQUENCE_BIT = 12; // 序列号占用的位数
    private final long MACHINE_BIT = 5;   // 机器标识占用的位数

    // 每部分的最大值
    private final long MAX_MACHINE_NUM = -1L ^ (-1L << MACHINE_BIT);
    private final long MAX_SEQUENCE_NUM = -1L ^ (-1L << SEQUENCE_BIT);

    // 每部分向左的位移
    private final long MACHINE_LEFT = SEQUENCE_BIT;
    private final long TIMESTAMP_LEFT = SEQUENCE_BIT + MACHINE_BIT;

    private final long machineId;  // 机器标识
    private AtomicLong sequence = new AtomicLong(0L); // 原子型序列号
    private volatile long lastTimestamp = -1L; // 上次生成ID的时间戳

    public SnowflakeIdGenerator(long machineId) {
        if (machineId > MAX_MACHINE_NUM || machineId < 0) {
            throw new IllegalArgumentException("Machine ID can't be greater than " + MAX_MACHINE_NUM + " or less than 0");
        }
        this.machineId = machineId;
    }

    // 生成唯一 ID
    public long generateId() {
        long currentTimestamp = System.currentTimeMillis();
        if (currentTimestamp < lastTimestamp) {
            throw new RuntimeException("Clock moved backwards. Refusing to generate ID");
        }

        // 在同一毫秒内自增序列号
        long sequenceValue = sequence.incrementAndGet() & MAX_SEQUENCE_NUM;
        if (sequenceValue == 0) {
            // 同一毫秒内的序列号已经达到最大值，等待下一毫秒
            currentTimestamp = nextMillis(lastTimestamp);
        }

        // 更新上次生成ID的时间戳
        lastTimestamp = currentTimestamp;

        // 构造ID
        return ((currentTimestamp - START_TIMESTAMP) << TIMESTAMP_LEFT)
                | (machineId << MACHINE_LEFT)
                | sequenceValue;
    }

    private long nextMillis(long lastTimestamp) {
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }

    // 测试
    public static void main(String[] args) {
        // 创建两个实例来模拟两台机器
        SnowflakeIdGenerator idGenerator1 = new SnowflakeIdGenerator(1);
        SnowflakeIdGenerator idGenerator2 = new SnowflakeIdGenerator(2);

        // 生成一些ID
        for (int i = 0; i < 10; i++) {
            long id1 = idGenerator1.generateId();
            long id2 = idGenerator2.generateId();
            System.out.println("Generated ID (Machine 1): " + id1);
            System.out.println("Generated ID (Machine 2): " + id2);
        }
    }
}
