package com.hurding.segment.order.entity;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Sharding {

    /**
     * 要识别的信息
     */
    String name() default "";

}
