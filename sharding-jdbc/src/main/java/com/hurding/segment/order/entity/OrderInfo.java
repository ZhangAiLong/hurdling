package com.hurding.segment.order.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import groovy.transform.ToString;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderInfo {

    /** 主键ID**/
    @Sharding
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 订单编码**/
    private String orderCode;

    /** 订单时间**/
    private Date orderDate;

    /** 订单状态**/
    private Integer orderStatus;
}