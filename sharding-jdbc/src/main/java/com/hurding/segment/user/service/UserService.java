package com.hurding.segment.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hurding.segment.user.entity.User;

import java.util.List;

public interface UserService extends IService<User> {

    void saveUser(User user);

    List<User> getUserByIdNoCache(Long id);

    User getUserById(Long id);

    User updateUser(User user);

    List<User> getUserByIdAndName(Long id, String userName);

    List<User> getUser(User user);

    String deleteUserById(Long id);
}
