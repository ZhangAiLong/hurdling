package com.hurding.segment.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hurding.segment.user.entity.User;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserMapper extends BaseMapper<User> {


}
