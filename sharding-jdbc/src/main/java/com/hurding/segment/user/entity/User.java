package com.hurding.segment.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value ="user")
public class User implements Serializable {
    private static final long serialVersionUID = 337361630075002456L;

    @TableId(type = IdType.INPUT)
    private Long cid;

    @TableField("name")
    private String name;

    @TableField("gender")
    private String gender;

    @TableField("data")
    private String data;

    public User(String name,String gender, String data) {
        this.name = name;
        this.data = data;
    }
}