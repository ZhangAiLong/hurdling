package com.hurding.segment.user.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hurding.segment.user.entity.User;
import com.hurding.segment.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/test")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @GetMapping("/insertTest")
    public void insertTest(){
        for (int i = 1 ; i < 10; i++) {
            User test = new User("王"+i,"男","数据" + i);
            userMapper.insert(test);
        }
    }

    @GetMapping("/selectOneTest")
    public User selectOneTest(Long userId){

        User user = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getCid,userId));
        System.out.println(JSON.toJSONString(user));
        return user;

    }

    @GetMapping("/selectListTest")
    public List<User> selectListTest(){
        List<User> userList = userMapper.selectList(null);
        System.out.println(JSON.toJSONString(userList));
        return userList;

    }
    @GetMapping("/selectListPage")
    public  List<User> selectListPage(){
        IPage<User> page = new Page(1,6);
        IPage<User> userIPage = userMapper.selectPage(page,null);
        List<User> records = userIPage.getRecords();
        System.out.println(records);
        return records;
    }


    @GetMapping("/selectListByGender")
    public void selectListByGender() {
        List<User> list = userMapper.selectList(Wrappers.<User>lambdaQuery().eq(User::getGender, "女"));
        System.out.println(list);
    }

    @GetMapping("/selectInList")
    public  List<User> selectList(){
        List<User> users = userMapper.selectList(Wrappers.<User>lambdaQuery().in(User::getCid,984009759378112513L,984009759428444160L));
        return users;
    }
}