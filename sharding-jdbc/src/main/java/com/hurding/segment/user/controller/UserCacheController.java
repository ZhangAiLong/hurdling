package com.hurding.segment.user.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hurding.segment.common.web.R;
import com.hurding.segment.user.entity.User;
import com.hurding.segment.user.mapper.UserMapper;
import com.hurding.segment.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/usercache")
public class UserCacheController {
    @Autowired
    private UserService userService;


    @PostMapping("save")
    public R saveUser(@RequestBody User user) {
        userService.saveUser(user);
        return R.success(null);
    }


    @GetMapping("getById")
    public R getById(@RequestParam Long id) {
        User user = userService.getUserById(id);
        return R.success(user);
    }

    @GetMapping("getByIdNoCache")
    public R getByNameNoCache(@RequestParam Long id) {
        List<User> users = userService.getUserByIdNoCache(id);
        return R.success(users);
    }


    @PostMapping("updateUser")
    public R updateUser(User user) {
        return R.success(userService.updateUser(user));
    }


    @PostMapping("deleteUserById")
    public R deleteUserById(Long id) {
        return R.success(userService.deleteUserById(id));
    }
}