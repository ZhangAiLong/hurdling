package com.hurding.segment.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hurding.segment.user.entity.User;
import com.hurding.segment.user.mapper.UserMapper;
import com.hurding.segment.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);


    @Override
    public void saveUser(User user) {
        save(user);
    }

    @Override
    public List<User> getUserByIdNoCache(Long id) {
        LambdaQueryWrapper<User> queryWrapper = Wrappers.<User>lambdaQuery().like(Objects.nonNull(id), User::getCid, id);
        List<User> users = list(queryWrapper);
        log.info("从数据库中读取，而非从缓存读取!");
        log.info("users: {}", users);
        return users;
    }

    @Override
    @Cacheable(cacheNames = "user", key = "#id")
    public User getUserById(Long id) {
        User user = getById(id);
        // 如果用户信息不为空，则加入缓存
//        if (user != null) {
//            caffeineCache.put(String.valueOf(user.getId()), user);
//        }

        log.info("从数据库中读取，而非从缓存读取!");
        log.info("users: {}", user);
        return user;
    }

    @Override
    @CachePut(cacheNames = "user", key = "#result.id")
    public User updateUser(User user) {
        log.info("user: {}", user);
        updateById(user);
        User user1 = getById(user.getCid());
        // 替换缓存中的值
        // caffeineCache.put(String.valueOf(user1.getId()), user1);

        return user1;
    }

    @Override
    @Cacheable(cacheNames = "user", key = "T(String).valueOf(#id).concat('::').concat(#userName)")
    public List<User> getUserByIdAndName(Long id, String userName) {
        LambdaQueryWrapper<User> queryWrapper = Wrappers.<User>lambdaQuery()
                .like(StringUtils.isNotBlank(userName), User::getName, userName)
                .eq(Objects.nonNull(id), User::getCid, id);
        List<User> users = list(queryWrapper);
        log.info("从数据库中读取，而非从缓存读取!");
        return users;
    }

    @Override
    @Cacheable(cacheNames = "user", key = "#user.userName")
    public List<User> getUser(User user) {
        LambdaQueryWrapper<User> queryWrapper = Wrappers.<User>lambdaQuery().like(StringUtils.isNotBlank(user.getName()), User::getName, user.getName());
        List<User> users = list(queryWrapper);
        log.info("从数据库中读取，而非从缓存读取!");
        return users;
    }

    @Override
    @CacheEvict(cacheNames = "user", beforeInvocation = true, key = "#id")
    public String deleteUserById(Long id) {
        boolean b = removeById(id);
//        if (b) {
//            // 从缓存中删除
//            caffeineCache.asMap().remove(String.valueOf(id));
//        }
        //  int i = 1 / 0;
        return b ? "删除成功" : "删除失败";
    }
}
