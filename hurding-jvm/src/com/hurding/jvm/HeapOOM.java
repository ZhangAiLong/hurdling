package com.hurding.jvm;


import java.util.ArrayList;
import java.util.List;

/**
 * 照成： Exception in thread "main" java.lang.OutOfMemoryError: Java heap space 异常
 * 设置 com.hurding.jvm  -Xms50M -Xmx50M
 */
public class HeapOOM {
    static class OOMObject {

    }

    public static void main(String[] args) {
        List<OOMObject> oomObjectList = new ArrayList<OOMObject>();
        while (true) {
            oomObjectList.add(new OOMObject());
        }
    }
}
