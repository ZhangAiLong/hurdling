package com.hurding.jvm;

import java.util.ArrayList;

/**
 * -XX:+PrintCommandLineFlags  查看进程的jvm参数
 * -XX:+UseSerialGC 设置垃圾回收器年轻代使用SerialGc，老年代使用SerialGC
 * Jdk1.8 使用的 -XX:+UseParallelGC
 */
public class SerialGcTest {

    public static void main(String[] args) {
        System.out.println("SerialGcTest......");
        ArrayList<byte[]> list = new ArrayList<byte[]>();
        while (true) {
            byte[] arr = new byte[1024];
            list.add(arr);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
