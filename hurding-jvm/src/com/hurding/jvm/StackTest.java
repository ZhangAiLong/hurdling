package com.hurding.jvm;

import java.nio.charset.StandardCharsets;

public class StackTest {

    public static void main(String[] args) {
        StackTest stackTest = new StackTest();
        stackTest.m1();


    }

    public void m1() {
        String str = "国家统计局数据显示，4月我国社会消费品零售总额同比下降11.1%。对此，商务部新闻发言人束珏婷5月19日表示，总体看，疫情冲击影响是阶段性的、暂时的，我国消费韧性强、潜力足的特点没有改变，消费发展长期向好的基本面没有改变";
        for (int i = 0; i < 1000; i++) {
            byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
            int total = bytes.length/1024/1024;
            System.out.println(total+"kb");      //1248kb  超过栈的内存 1M 异常：java.lang.OutOfMemoryError: Requested array size exceeds VM limit
            str += str;
        }
    }
}
