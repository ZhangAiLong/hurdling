package com.hurding.jvm.jstack;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 使用jstack定位 线程wite
 */
public class Jvm_jstatck_02 {

    public static void main(String[] args) {
        ExecutorService ex = Executors.newFixedThreadPool(1);
        ex.execute(new TestTask());

    }

}

class TestTask implements Runnable{


    @Override
    public void run() {
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}