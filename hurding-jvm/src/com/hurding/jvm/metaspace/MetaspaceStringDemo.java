package com.hurding.jvm.metaspace;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 虚拟机加载的类信息
 * 常量池
 * 即时编译后的代码
 * 静态变量
 *  jdk1.6 -XX:PermSize=10m -XX:MaxPermSize=10m
 */
public class MetaspaceStringDemo {

    static class OOM{}

    public static void main(String[] args) {
        int i=0;
        List<String> list = new ArrayList<String>();

        try {
           while (true) {
               i=i+1;
               list.add(UUID.randomUUID().toString().intern());
           }
        }catch (Exception  ex) {
            System.out.println("=================多少次后发生异常："+i);
        }
    }
}
