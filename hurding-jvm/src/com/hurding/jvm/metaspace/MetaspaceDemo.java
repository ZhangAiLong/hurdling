package com.hurding.jvm.metaspace;

/**
 * 虚拟机加载的类信息
 * 常量池
 * 即时编译后的代码
 * 静态变量
 *
 */
public class MetaspaceDemo {

    static class OOM{}

    public static void main(String[] args) {
        int i=0;

        try {
           while (true) {
               i=i+1;

           }
        }catch (Exception  ex) {
            System.out.println("=================多少次后发生异常："+i);
        }
    }
}
