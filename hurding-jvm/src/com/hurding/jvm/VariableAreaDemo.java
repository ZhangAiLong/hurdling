package com.hurding.jvm;


/**
 * 问题：成员变量、局部变量、类变量分别存储在内存的什么
 * 地方？
 */
public class VariableAreaDemo {
    //成员变量   是对象实例的一部分，存储在堆内存，随着实例的回收而回收。
    private String v1;
    //类变量  存储在堆内存中，jdk1.8之前存储在方法区，jdk1.8之后放到堆内存
    static String v2="23423423";

    public static void main(String[] args) throws InterruptedException {
        //局部变量: 存储在虚拟机栈帧中
        String v3 = "hello world";;
        System.out.println(v3);

        Thread.sleep(10000000);
    }
}
