package com.zhangal.tools.excel.dto;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @ClassName CityDto
 * @Description TODO
 * @Author HQJN
 * @Date 2022/2/19 14:57
 **/
public class CityDto {
    @JSONField(ordinal = 0)
    private Integer id;

    @JSONField(ordinal = 1)
    private String city;

    @JSONField(ordinal = 2)
    private String lon;


    @JSONField(ordinal = 3)
    private String lat;

    //[
//    {
//        "pname": "安徽",
//            "lon": 105,
//            "lat": 44,
//            "id": 0,
//            "cityList": [
//        {
//            "cityName": "安庆",
//                "lon": 105,
//                "lat": 44,
//                "cid": 0
//        }
//        ]
//    }
//]

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }



    public CityDto(){}

    public CityDto(Integer id, String city, String lon, String lat) {
        this.id = id;
        this.city = city;
        this.lon = lon;
        this.lat = lat;
    }
}
