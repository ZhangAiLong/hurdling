package com.zhangal.tools.excel.dto;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @ClassName ProvinceDto
 * @Description TODO
 * @Author HQJN
 * @Date 2022/2/19 15:00
 **/
public class ProvinceDto {
    @JSONField(ordinal = 0)
    private Integer id;

    @JSONField(ordinal = 1)
    private String province;

    @JSONField(ordinal = 2)
    private String lon;

    @JSONField(ordinal = 3)
    private String lat;

    @JSONField(ordinal = 4)
    private List<CityDto> cityList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public List<CityDto> getCityList() {
        return cityList;
    }

    public void setCityList(List<CityDto> cityList) {
        this.cityList = cityList;
    }




}
