package com.zhangal.tools.excel.dto;

import java.util.Comparator;

/**
 * @ClassName Comparator
 * @Description TODO
 * @Author HQJN
 * @Date 2022/2/19 15:54
 **/
public class StudentComparator implements Comparator<ProvinceDto> {
    @Override
    public int compare(ProvinceDto o1, ProvinceDto o2) {
        return o1.getId().compareTo(o2.getId());
    }
}
