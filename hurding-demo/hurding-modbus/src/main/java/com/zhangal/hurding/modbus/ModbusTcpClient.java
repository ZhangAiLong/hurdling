//package com.zhangal.hurding.modbus;
//
//public class ModbusTcpClient {
//
//    public static void main(String[] args) {
//        try {
//            // 设置 Modbus 设备的 IP 和端口号
//            String ip = "设备IP";
//            int port = 502;
//
//            // 创建 Modbus TCP 连接
//            TCPMasterConnection connection = new TCPMasterConnection(ip);
//            connection.setPort(port);
//            connection.connect();
//
//            // 创建一个读取保持寄存器的请求
//            ReadMultipleRegistersRequest req = new ReadMultipleRegistersRequest(1, 0, 1);
//            req.setUnitID(1);
//
//            // 创建 Modbus 事务并执行请求
//            ModbusTCPTransaction trans = new ModbusTCPTransaction(connection);
//            trans.setRequest(req);
//            trans.execute();
//
//            // 获取响应并输出结果
//            ReadMultipleRegistersResponse res = (ReadMultipleRegistersResponse) trans.getResponse();
//            if (res != null) {
//                System.out.println("读取到的值为: " + res.getRegisterValue(0));
//            } else {
//                System.out.println("未能成功读取保持寄存器的值");
//            }
//
//            // 关闭连接
//            connection.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}