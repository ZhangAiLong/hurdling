package com.zhangal.hurding.modbus;





import java.util.Objects;

/**
 * @author huangji
 * @version 1.0
 **/
public class ModbusNetworkAddress {
    private String ipAddr;
    private int port = ModbusMasterUtil.TCP_PORT;

    public ModbusNetworkAddress(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public ModbusNetworkAddress(String ipAddr, Integer port) {
        this.ipAddr = ipAddr;
        if (port != null) {
            this.port = port;
        }
    }

    public ModbusNetworkAddress(String ipAddr, String port) {
        this.ipAddr = ipAddr;
        if (port != null) {
            this.port = Integer.parseInt(port);
        }
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "ModbusNetwork{" +
                "ipAddr='" + ipAddr + '\'' +
                ", port=" + port +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModbusNetworkAddress that = (ModbusNetworkAddress) o;
        return port == that.port && ipAddr.equals(that.ipAddr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ipAddr, port);
    }
}