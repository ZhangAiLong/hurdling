package com.zhangal.hurding.basic.datatype;


/**
 * integer比较
 */
public class IntegerEqualsTest {

    public static void main(String[] args) {

        Integer a = new Integer(1);
        Integer b = new Integer(1);
        System.out.println("a==b = "+(a==b));
        System.out.println("a.equals(b) = "+(a.equals(b)));

        System.out.println("==================================================");

        Integer c = 1;
        Integer d = 1;
        System.out.println("c==d = "+(c==d));
        System.out.println("c.equals(d) = "+(c.equals(d)));

        System.out.println("==================================================");

        Integer e = new Integer(128);
        Integer f = new Integer(128);
        System.out.println("e==f = "+(e==f));
        System.out.println("e.equals(f) = "+(e.equals(f)));
        System.out.println("==================================================");

        Integer g = 128;
        Integer k = 128;
        System.out.println("g==k = "+(g==k));
        System.out.println("g.equals(k) = "+(g.equals(k)));

        System.out.println("Integer.valueOf(129): "+Integer.valueOf(129));
    }

}
