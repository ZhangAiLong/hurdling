//package com.zhangal.hurding.basic.count;
//
//import java.util.DoubleSummaryStatistics;
//
//public class AggregationDoubleStatistics extends DoubleSummaryStatistics {
//
//    private double m1;
//    private double m2;
//    private double m3;
//    private double m4;
//
//    @Override
//    public void accept(double x) {
//        super.accept(x);
//
//        long n = getCount();
//
//        //δ
//        double delta = x - m1;
//        //δ / n
//        double deltaN = delta / n;
//        //δ^2 / n
//        double delta2N = delta * deltaN;
//        //δ^2 / n^2
//        double delta2N2 = deltaN * deltaN;
//        //δ^3 / n^2
//        double delta3N2 = delta2N * deltaN;
//        //δ^4 / n^3
//        double delta4N3 = delta3N2 * deltaN;
//
//        m4 += (n - 1) * (n * n - 3 * n + 3) * delta4N3
//                + 6 * m2 * delta2N2
//                - 4 * m3 * deltaN;
//        m3 += (n - 1) * (n - 2) * delta3N2
//                - 3 * m2 * deltaN;
//        m2 += (n - 1) * delta2N;
//        m1 += deltaN;
//    }
//
//    @Override
//    public void combine(DoubleSummaryStatistics other) {
//        throw new UnsupportedOperationException(
//                "Can't combine a standard DoubleSummaryStatistics with this class");
//    }
//
//    public void combine(MoreDoubleStatistics other) {
//        MoreDoubleStatistics s1 = this;
//        MoreDoubleStatistics s2 = other;
//
//        long n1 = s1.n();
//        long n2 = s2.n();
//        long n = n1 + n2;
//
//        //δ
//        double delta = s2.m1 - s1.m1;
//        //δ / n
//        double deltaN = delta / n;
//        //δ^2 / n
//        double delta2N = delta * deltaN;
//        //δ^2 / n^2
//        double delta2N2 = deltaN * deltaN;
//        //δ^3 / n^2
//        double delta3N2 = delta2N * deltaN;
//        //δ^4 / n^3
//        double delta4N3 = delta3N2 * deltaN;
//
//        this.m4 = s1.m4 + s2.m4 + n1 * n2 * (n1 * n1 - n1 * n2 + n2 * n2) * delta4N3
//                + 6.0 * (n1 * n1 * s2.m2 + n2 * n2 * s1.m2) * delta2N2
//                + 4.0 * (n1 * s2.m3 - n2 * s1.m3) * deltaN;
//
//        this.m3 = s1.m3 + s2.m3 + n1 * n2 * (n1 - n2) * delta3N2
//                + 3.0 * (n1 * s2.m2 - n2 * s1.m2) * deltaN;
//
//        this.m2 = s1.m2 + s2.m2 + n1 * n2 * delta2N;
//
//        this.m1 = s1.m1 + n2 * delta;
//
//        super.combine(other);
//    }
//
//    private long n() {
//        return getCount();
//    }
//
//    /**
//     * 计算流的方差
//     * @return
//     */
//    public double getVariance() {
//        return n() <= 1 ? 0 : m2 / (n() - 1);
//    }
//
//    /**
//     * 计算流的标准差
//     * @return
//     */
//    public double getStdDev() {
//        return Math.sqrt(getVariance());
//    }
//
//    /**
//     * 计算流的偏度
//     * @return
//     */
//    public double getSkewness() {
//        return m2 == 0 ? 0 : Math.sqrt(n()) * m3 / Math.pow(m2, 1.5);
//    }
//
//    /**
//     * 计算流的峰度
//     * @return
//     */
//    public double getKurtosis() {
//        return m2 == 0 ? 0 : n() * m4 / (m2 * m2) - 3.0;
//    }
//}
