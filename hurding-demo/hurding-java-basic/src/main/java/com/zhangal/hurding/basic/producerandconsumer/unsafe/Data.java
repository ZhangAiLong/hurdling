package com.zhangal.hurding.basic.producerandconsumer.unsafe;

import java.io.Serializable;
import java.util.Date;

public class Data implements Serializable {

    private String name;

    private Double value;

    private Date time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
