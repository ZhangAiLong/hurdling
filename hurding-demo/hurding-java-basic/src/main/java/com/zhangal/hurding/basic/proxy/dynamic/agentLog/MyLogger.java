package com.zhangal.hurding.basic.proxy.dynamic.agentLog;

import java.lang.reflect.Method;

public interface MyLogger {

    //记录进入方法时间
    public void saveIntoMethodTime(Method method);


    //记录退出方法的时间
    public void saveOutMethodTime(Method method);
}
