package com.zhangal.hurding.basic.jvm;

import java.util.ArrayList;

/**
 * -XX:+PrintCommandLineFlags  查看进程的jvm参数
 * -XX:+UseG1GC 设置垃圾回收器G1Gc
 * Jdk1.8 使用的 -XX:+UseParallelGC
 */
public class UseG1GcTest {

    public static void main(String[] args) {
        System.out.println("UseG1GcTest......");
        ArrayList<byte[]> list = new ArrayList<byte[]>();
        while (true) {
            byte[] arr = new byte[1024];
            list.add(arr);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
