package com.zhangal.hurding.basic.proxy.dynamic.countTime.proxy;

import com.zhangal.hurding.basic.proxy.dynamic.countTime.factory.IUserServiceProxyFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class UserServiceProxy implements IUserServiceProxy {





    public Object proxy(Object obj) {

        Object instance =(Object) Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("==========start metond==========");
                long b = System.currentTimeMillis();
                Object result = null;
                try {
                    result = method.invoke(obj, args);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                System.out.println("执行方法耗时：" + (System.currentTimeMillis() - b) + "ms");
                System.out.println("==========end metond==========");
                return result;
            }
        });

           return instance;
    }

}
