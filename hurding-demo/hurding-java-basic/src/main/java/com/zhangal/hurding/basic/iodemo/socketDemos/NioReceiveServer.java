package com.zhangal.hurding.basic.iodemo.socketDemos;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName NioReceiveServer
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/3 20:46
 **/
public class NioReceiveServer {
    //接受文件路径
    private static final String RECEIVE_PATH = "/";

    static class Session {
        int step = 1; //1 读取文件名称的长度，2 读取文件名称  ，3 ，读取文件内容的长度， 4 读取文件内容
        //文件名称
        String fileName = null;

        //长度
        long fileLength;
        int fileNameLength;

        //开始传输的时间
        long startTime;

        //客户端的地址
        InetSocketAddress remoteAddress;

        //输出的文件通道
        FileChannel fileChannel;

        //接收长度
        long receiveLength;

        public boolean isFinished() {
            return receiveLength >= fileLength;
        }

    }
    private Charset charset = Charset.forName("UTF-8");

    private ByteBuffer buffer
            = ByteBuffer.allocate(10240);

    //使用Map保存每个客户端传输，当OP_READ通道可读时，根据channel找到对应的对象
    Map<SelectableChannel, Session> clientMap = new HashMap<SelectableChannel, Session>();

    public void startServer() throws IOException {
        // 1.获取选择器
        Selector selector = Selector.open();
        // 2、获取通道
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        ServerSocket socket = serverChannel.socket();

        //绑定连接
        InetSocketAddress inetSocketAddress = new InetSocketAddress(12306);
        socket.bind(inetSocketAddress);

        //5、讲通道注册到选择器内,并注册的IO事件为：“接收新连接”
        serverChannel.register(selector,SelectionKey.OP_ACCEPT);
        System.out.println("serverChannel is linstening...");
        //6、6、轮询感兴趣的I/O就绪事件（选择键集合）

        while (selector.select()>0) {
            if (selector.selectedKeys()==null) {
                continue;
            }
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();
                if (selectionKey==null) {
                    continue;
                }
                // 9、判断key是具体的什么事件，是否为新连接事件
                if (selectionKey.isAcceptable()) {
                    ServerSocketChannel serverSocketChannel = (ServerSocketChannel)selectionKey.channel();
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    if (socketChannel == null) continue;
                    socketChannel.configureBlocking(false);
                    socketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);

                    socketChannel.register(selector,SelectionKey.OP_READ);

                    // 余下为业务处理
                    Session session = new Session();
                    session.remoteAddress
                            = (InetSocketAddress) socketChannel.getRemoteAddress();
                    clientMap.put(socketChannel, session);
                    System.out.println(socketChannel.getRemoteAddress() + "连接成功...");
                } else if (selectionKey.isReadable()) {
                    handleData(selectionKey);
                }

                // NIO的特点只会累加，已选择的键的集合不会删除
                // 如果不删除，下一次又会被select函数选中
                iterator.remove();
            }

        }

    }

    public void handleData(SelectionKey selectionKey) throws IOException{
        SocketChannel socketChannel = (SocketChannel)selectionKey.channel();

        int num = 0;
        Session session = clientMap.get(selectionKey.channel());
        buffer.clear();
        while ((num = socketChannel.read(buffer))>0) {
            System.out.println("收到的字节数 = "+num);
            buffer.flip();
            process(session,buffer);
            buffer.clear();
        }

    }

    private void process(Session session,ByteBuffer buffer) {
       while ( buffer.remaining() > 0) {
           if (session.step==1) {
               int fileNameLengthByteLen  = len(buffer);
               System.out.println("读取文件名称长度之前，可读取的字节数 = " + fileNameLengthByteLen);
               System.out.println("读取文件名称长度之前，buffer.remaining() = " + buffer.remaining());
               System.out.println("读取文件名称长度之前，buffer.capacity() = " + buffer.capacity());
               System.out.println("读取文件名称长度之前，buffer.limit() = " + buffer.limit());
               System.out.println("读取文件名称长度之前，buffer.position() = " + buffer.position());

               if (len(buffer) < 4) {
                   System.out.println("出现半包问题，需要更加复制的拆包方案");
                   throw new RuntimeException("出现半包问题，需要更加复制的拆包方案");
               }

               //获取文件名称长度
               session.fileNameLength = buffer.getInt();

               System.out.println("读取文件名称长度之后，buffer.remaining() = " + buffer.remaining());
               System.out.println("读取文件名称长度 = " + session.fileNameLength);

               session.step = 2;
           }else if (session.step==2) {
               System.out.println("step2.....");
               if (len(buffer) < session.fileNameLength) {
                   System.out.println("出现半包问题，需要更加复制的拆包方案");
                   throw new RuntimeException("出现半包问题，需要更加复制的拆包方案");
               }


           } else if (session.step==3) {

           } else if (session.step==4) {

           }

       }


    }

    private static int len(ByteBuffer buffer) {

        System.out.println(" >>>  buffer left：" + buffer.remaining());
        return buffer.remaining();
    }
}
