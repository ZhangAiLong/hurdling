package com.zhangal.hurding.basic.proxy.dynamic.countTime.proxy;

public interface IUserServiceProxy {

    public Object proxy(Object obj);
}
