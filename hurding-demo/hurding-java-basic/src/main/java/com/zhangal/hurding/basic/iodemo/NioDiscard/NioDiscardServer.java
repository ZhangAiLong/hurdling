package com.zhangal.hurding.basic.iodemo.NioDiscard;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * @ClassName NioDiscardServer
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/3 14:31
 **/
public class NioDiscardServer {
    public static void startServer() throws IOException{

        //1.获取选择器
        Selector selector = Selector.open();
        //2.获取通道
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //3.设置非阻塞
        serverSocketChannel.configureBlocking(false);
        //4.绑定连接
        serverSocketChannel.bind(new InetSocketAddress(18899));
        System.out.println("服务器启动:");
        //5.将通道注册的“接收新连接”IO事件注册到选择器上
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        //6.轮询感兴趣的IO就绪事件（选择键集合）
        while ( selector.select() > 0 ) {
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                //8.获取单个的选择键，并处理
                SelectionKey selectionKey = iterator.next();
                //9.判断key是具体的什么事件
                if (selectionKey.isAcceptable()) {
                    //10.若选择键的IO事件是“连接就绪”，就获取客户端连接
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    //11.将新连接切换为非阻塞模式
                    socketChannel.configureBlocking(false);
                    //12.将新连接的通道的可读事件注册到选择器上
                    socketChannel.register(selector,SelectionKey.OP_READ);
                    //13.若选择键的IO事件是“可读”，则读取数据
                } else if (selectionKey.isReadable()) {
                    SocketChannel socketChannel = (SocketChannel)selectionKey.channel();
                    //14.读取数据，然后丢弃
                    ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                    int length = 0;
                    while ((length = socketChannel.read(byteBuffer))>0) {
                        byteBuffer.flip();
                        System.out.println(new String(byteBuffer.array(), 0,
                                length));
                        byteBuffer.clear();

                    }
                    socketChannel.close();
                }
                //15.移除选择键
                iterator.remove();

            }
            //7.关闭连接
            serverSocketChannel.close();
        }


    }


    public static void main(String[] args) throws IOException {
        startServer();
    }
}
