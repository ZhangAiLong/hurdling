package com.zhangal.hurding.basic.producerandconsumer.unsafe;

import lombok.SneakyThrows;

import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class DataBufferUnsafe<T> {

    private static final Integer limitNum = 30;


    private  LinkedList<T> dataList  = new LinkedList<T>();


    private AtomicInteger amount  = new AtomicInteger(0);

    public void add (T element) throws Exception {
        if(amount.get() > limitNum) {
            System.out.println("队列已经满了啊！");
            return;
        }

        dataList.add(element);
        amount.incrementAndGet();
        if (amount.get() != dataList.size()) {
            throw  new Exception(amount+ "!=" + dataList.size() );
        }
    }


    public T feth() throws Exception {
        if (amount.get() <= 0) {
            System.out.println("队列已经空了！");
            return null;
        }
        T element = dataList.remove(0);
        amount.decrementAndGet();
        System.out.println(element+"");
        if (amount.get()!= dataList.size()) {
           throw new Exception(amount + "!= " +dataList.size());
        }
        return element;

    }





    Runnable product = new Runnable() {
        Random random = new Random();

        @SneakyThrows
        @Override
        public void run() {
           while (dataList.size()<30) {
//               Data data = new Data();
//               data.setName("name1");
//               data.setTime(new Date());
//               data.setValue(random.nextDouble());
//               dataList.add(data);
               Thread.sleep(100);
           }

        }
    };

    Runnable consume = new Runnable() {
        @Override
        public void run() {
            while (dataList.size()>0) {
                Data peek = (Data) dataList.peek();
                System.out.println(peek.getValue());
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

}
