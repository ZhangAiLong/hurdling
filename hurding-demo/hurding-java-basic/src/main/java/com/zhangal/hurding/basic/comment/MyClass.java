package com.zhangal.hurding.basic.comment;

/**
 * 这是一个类的文档注释示例
 * 描述类的功能和用途
 */
public class MyClass {

    // 示例单行注释
    int x = 10; // 初始化变量x为10
 
    /*
     * 这是一个多行注释示例
     * 可以包含多个段落的解释说明
     */
    int y = 20;
    /**
     * 这是一个计算两个数之和的方法
     *
     * @param a 第一个整数
     * @param b 第二个整数
     * @return 两个数的和
     */
    public int sum(int a, int b) {
        return a + b;
    }

    /**
     * 这是一个用于计算阶乘的方法
     * 使用递归方式实现
     * @param n 需要计算阶乘的正整数
     * @return 计算得到的阶乘结果
     * @throws IllegalArgumentException 如果 n 小于0，则抛出此异常
     */
    public int factorial(int n) throws IllegalArgumentException {
        if (n < 0) {
            throw new IllegalArgumentException("n不能为负数");
        }
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    /**
     * 这是一个方法的文档注释示例
     * @param a 参数a的说明
     * @param b 参数b的说明
     * @return 返回值的说明
     */
    public int myMethod(int a, int b) {
        return a + b;
    }


}
