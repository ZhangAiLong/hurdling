package com.zhangal.hurding.basic.proxy.dynamic.countTime;

import com.zhangal.hurding.basic.proxy.dynamic.countTime.factory.UserServiceProxyFactory;
import com.zhangal.hurding.basic.proxy.dynamic.countTime.proxy.UserServiceProxy;
import com.zhangal.hurding.basic.proxy.dynamic.countTime.service.IUserService;
import com.zhangal.hurding.basic.proxy.dynamic.countTime.service.UserService;

public class CountTimeTest {
    public static void main(String[] args) {

        IUserService userService = new UserService();
//        userService.findAll();

        UserServiceProxy userServiceProxy = UserServiceProxyFactory.getInstance().createUserServiceProxy();
        IUserService userService2 = (IUserService)userServiceProxy.proxy(userService);
//        userService2.getUserById(1);
//        System.out.println(userService2);
        userService2.findAll();
    }
}
