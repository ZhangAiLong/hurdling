package com.zhangal.hurding.basic.proxy.dynamic.countTime.service;

import com.zhangal.hurding.basic.proxy.dynamic.countTime.dto.UserDto;

import java.util.List;

public interface IUserService {

    public List<UserDto> findAll();


    public UserDto getUserById(Integer id);
}
