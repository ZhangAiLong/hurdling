package com.zhangal.hurding.basic.socket;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class UploadServer {

    public static void main(String[] args) throws IOException, IOException {

//1.创建server
        ServerSocket server = new ServerSocket(8080);
//2.监听
        Socket socket = server.accept();
//3.通过socket读取网络文件
       /*InputStream in = socket.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(in);*/
        BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
//4.写入到本地文件
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("D:\\Demo\\Java\\IDEA\\RandomAccessFile-0721\\recv.mp4"));
//5.边读边写
//读取的中转站
        byte[] buf = new byte[80 * 1024];

        int len = 0;

        int count = 0;
//len = bis.read(buf);
        while ((len = bis.read(buf)) != -1) {//组合式写法
//写入到文件
            bos.write(buf, 0, len);
            count += 1;
//再次读取
//len = bis.read(buf);
            System.out.println("已收到：" + count);
        }
//关闭流
        bos.close();

        bis.close();

    }

}
