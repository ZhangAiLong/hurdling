package com.zhangal.hurding.basic.proxy.dynamic.agentLog;

import java.lang.reflect.Proxy;

public class MyLoggerTest {

    public static void main(String[] args) {
        /** 实例化真实项目中业务类 **/
        IBusinessClassService businessClassService = new BusinessClassServiceImpl();
        /** 日志类的handler **/
        MyLoggerHandler myLoggerHandler = new MyLoggerHandler(businessClassService);
        /** 获得代理类对象 **/
        IBusinessClassService businessClass = (IBusinessClassService) Proxy.newProxyInstance(businessClassService.getClass().getClassLoader(), businessClassService.getClass().getInterfaces(),myLoggerHandler);
        /** 执行代理类方法 **/
        businessClass.doSomeThing();

    }

}
