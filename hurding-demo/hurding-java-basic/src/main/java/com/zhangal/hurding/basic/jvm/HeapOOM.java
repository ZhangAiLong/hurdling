package com.zhangal.hurding.basic.jvm;


import com.google.common.collect.Lists;

import java.util.List;

/**
 * 照成： Exception in thread "main" java.lang.OutOfMemoryError: Java heap space 异常
 * 设置 jvm  -Xms50M -Xmx50M
 */
public class HeapOOM {
    static class OOMObject {

    }

    public static void main(String[] args) {
        List<OOMObject> oomObjectList = Lists.newArrayList();
        while (true) {
            oomObjectList.add(new OOMObject());
        }
    }
}
