package com.zhangal.hurding.basic.iodemo.bio;

import java.io.*;
import java.net.Socket;

public class BIOClientSocket {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1",8080);
        //使用BufferedWriter，像服务器端写入一个消息
        BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        bufferedWriter.write("我是客户端Client-01\n");
        bufferedWriter.flush();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String serverStr=bufferedReader.readLine(); //通过bufferedReader读取服务端返回的消息
        System.out.println("服务端返回的消息："+serverStr);

    }
}
