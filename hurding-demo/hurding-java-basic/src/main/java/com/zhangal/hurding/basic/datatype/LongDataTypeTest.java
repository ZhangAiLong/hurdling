package com.zhangal.hurding.basic.datatype;

public class LongDataTypeTest {

    public static void main(String[] args) {
       Long number = 100L;


       System.out.println("main 1 number="+number);
        LongDataTypeTest longDataTypeTest = new LongDataTypeTest();
        longDataTypeTest.testLongTypePass(number);
        System.out.println("main 2 number="+number);


        User a = new User("A");
        User b = new User("B");
        swapName(a,b);

        System.out.println("a="+a.toString());
        System.out.println("b="+b.toString());

    }

    public void testLongTypePass(Long number) {
        number +=10;
        System.out.println("test number="+number);

    }

    private static void swap(User a, User b) {
        User tmp = a;
        a = b;
        b = tmp;
        System.out.println(" a swap="+a);
        System.out.println(" b swap="+b);
        a = new User("c");

    }

    public static void swapName(User a,User b) {
        User tmp = a;

        a = b;
        b = tmp;
        System.out.println(" a swap="+a);
        System.out.println(" b swap="+b);
        b.setName("cccccc");
    }

}


class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}