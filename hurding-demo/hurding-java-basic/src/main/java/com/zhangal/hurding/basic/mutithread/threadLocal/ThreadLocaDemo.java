package com.zhangal.hurding.basic.mutithread.threadLocal;

public class ThreadLocaDemo {

    private static ThreadLocal<String> localVar = new ThreadLocal<String>();
    private static ThreadLocal<Integer> localInt = new ThreadLocal<Integer>();

    static void print(String str) {
        System.out.println(str+":" + localVar.get());
        System.out.println("Int:" + localInt.get());
        localVar.remove();
    }


    public static void main(String[] args) throws InterruptedException {
        new Thread(new Runnable() {
            public void run() {
                ThreadLocaDemo.localVar.set("local_A");

                //打印本地变量
                System.out.println("after remove : " + localVar.get());
                int n = 0;
                for (int i = 0; i <100 ; i++) {
                    n = n + i;
                    localInt.set(n);
                }
                print("A");
            }
        },"A").start();

//        Thread.sleep(1000);

        new Thread(new Runnable() {
            public void run() {
                ThreadLocaDemo.localVar.set("local_B");

                System.out.println("after remove : " + localVar.get());
                int n = 0;
                for (int i = 0; i <100 ; i++) {
                    n = n + i;
                    localInt.set(n);
                }
                print("B");
            }
        },"B").start();

    }


}
