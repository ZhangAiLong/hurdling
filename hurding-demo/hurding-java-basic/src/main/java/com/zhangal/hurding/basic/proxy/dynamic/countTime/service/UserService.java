package com.zhangal.hurding.basic.proxy.dynamic.countTime.service;

import com.zhangal.hurding.basic.proxy.dynamic.countTime.dto.UserDto;

import java.util.List;

public class UserService implements IUserService{
    @Override
    public List<UserDto> findAll() {
        System.out.println("findAll......");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserDto getUserById(Integer id) {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("getUserById......");
        return null;
    }
}
