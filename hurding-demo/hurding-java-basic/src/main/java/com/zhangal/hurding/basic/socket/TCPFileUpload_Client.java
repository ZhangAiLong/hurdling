package com.zhangal.hurding.basic.socket;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * 客户端
 */
public class TCPFileUpload_Client {
    public static void main(String[] args) throws Exception {
        //连接服务端 (ip , 端口）
        //解读:连接本机的 8888端口, 如果连接成功，返回Socket对象
        Socket socket = new Socket("192.168.56.121", 8888);
        System.out.println("客户端 连接端口:" + socket.getPort());

        //创建读取磁盘文件IO流
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("d://Download/401.png"));
        //获取字节数组
        byte[] bytes = StreamUtils.streamToByteArray(bufferedInputStream);
        //通过Socket获取到输出流,将bytes发送到服务端
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
        bufferedOutputStream.write(bytes);
        //关闭流对象,socket,刷新，添加终止符
        bufferedInputStream.close();
        bufferedOutputStream.flush();
        socket.shutdownOutput();

        //接受回复消息
        //此处可调用Utils的方法
//        String s = "";
//        while ((s = bufferedReader.readLine()) != null)
//            System.out.println(s);
        System.out.println(StreamUtils.streamToString(socket.getInputStream()));

        //关闭所有流
        bufferedOutputStream.close(); //socket的包装流不要过早关闭
        socket.close();

    }

}