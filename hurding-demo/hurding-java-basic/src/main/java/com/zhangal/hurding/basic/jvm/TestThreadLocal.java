package com.zhangal.hurding.basic.jvm;

public class TestThreadLocal {

    public static void main(String[] args) {
        A a = new A();
        a.getLocal();
        System.gc();;
        Thread thread = Thread.currentThread();

    }
}

class A{

    public ThreadLocal<String> getLocal() {
        ThreadLocal threadLocal = new ThreadLocal();
        threadLocal.set("zhangl");
        return threadLocal;
    }

}
