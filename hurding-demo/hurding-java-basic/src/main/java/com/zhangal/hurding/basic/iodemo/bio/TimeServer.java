package com.zhangal.hurding.basic.iodemo.bio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeServer {

    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");

    public static void main(String[] args) {
        int port = 8181;
        if (args!=null && args.length>0) {
            port = Integer.valueOf(args[0]);
        }
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(port);
            System.out.println("This time server is start in port:"+port);
            Socket socket = null;
            while (true) {
                socket = serverSocket.accept();;
                new Thread(new TimeServerHandler(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static class TimeServerHandler implements Runnable{

       private Socket socket;

        public TimeServerHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            BufferedReader in = null;
            PrintWriter out = null;
            try {
                in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
                out = new PrintWriter(this.socket.getOutputStream(),true);
                String body = null;
                String currentTime = null;
                while (true) {
                    body =  in.readLine();
                    System.out.println(body);
                    if (body==null) {
                        break;
                    }
                    currentTime = "QUERY TIME ORDER".equalsIgnoreCase(body)?simpleDateFormat.format(new Date(System.currentTimeMillis())):"BAD ORDER";
                    out.println(currentTime);
                }

            } catch (IOException e) {
                if (out !=null ) {
                    out.close();
                }
                if (in !=null ) {
                    try {
                        in.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
                if (socket!=null && socket.isConnected()) {
                    try {
                        socket.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
                e.printStackTrace();
            }
        }
    }
}
