package com.zhangal.hurding.basic.datatype;

import org.junit.Test;

import java.io.UnsupportedEncodingException;

/**
 * @ClassName DataTypeByteLengthTester
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/10 14:55
 **/
public class DataTypeByteLengthTester {


    @Test
    public void test1() throws UnsupportedEncodingException {

        String a = "名";
        char b = '中';

        System.out.println("UTF-8编码长度:"+a.getBytes("UTF-8").length);

        System.out.println("GBK编码长度:"+a.getBytes("GBK").length);

        System.out.println("GB2312编码长度:"+a.getBytes("GB2312").length);

        System.out.println("char编码长度:"+charToByte(b).length);


        System.out.println("==========================================");

        String c = "0x20001";

        System.out.println("UTF-8编码长度:"+c.getBytes("UTF-8").length);

        System.out.println("GBK编码长度:"+c.getBytes("GBK").length);

        System.out.println("GB2312编码长度:"+c.getBytes("GB2312").length);

        System.out.println("==========================================");

        char[] arr = Character.toChars(0x20001);

        String s = new String(arr);

        System.out.println("char array length:" + arr.length);

        System.out.println("content:| " + s + " |");

        System.out.println("String length:" + s.length());

        System.out.println("UTF-8编码长度:"+s.getBytes("UTF-8").length);

        System.out.println("GBK编码长度:"+s.getBytes("GBK").length);

        System.out.println("GB2312编码长度:"+s.getBytes("GB2312").length);

        System.out.println("==========================================");

    }

    public static byte[] charToByte(char c) {
        byte[] b = new byte[2];
        b[0] = (byte) ((c & 0xFF00) >> 8);
        b[1] = (byte) (c & 0xFF);
        return b;

    }
}
