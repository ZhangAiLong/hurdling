package com.zhangal.hurding.basic.jvm;

public class GcRefe {

    public static void main(String[] args) {

        Test object1 = new Test();
        Test object2 = new Test();
        object2.object = object2;
        object2.object = object1;
        object1 = null;
        object2 = null;

    }


}

class Test {
    public Test object = null;
}
