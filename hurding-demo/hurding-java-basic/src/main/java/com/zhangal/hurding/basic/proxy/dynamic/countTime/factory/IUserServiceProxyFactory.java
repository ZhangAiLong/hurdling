package com.zhangal.hurding.basic.proxy.dynamic.countTime.factory;

import com.zhangal.hurding.basic.proxy.dynamic.countTime.proxy.UserServiceProxy;

public interface IUserServiceProxyFactory {

    public UserServiceProxy createUserServiceProxy();

}
