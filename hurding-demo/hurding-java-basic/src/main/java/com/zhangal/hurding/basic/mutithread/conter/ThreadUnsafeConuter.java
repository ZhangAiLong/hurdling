package com.zhangal.hurding.basic.mutithread.conter;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadUnsafeConuter {


    private static Integer counter = 0;


    public static void main(String[] args) throws InterruptedException {

//        ExecutorService executor = Executors.newFixedThreadPool(10);
//        for (int i = 0; i < 10 ; i++) {
//            executor.submit(new ThreadConuter());
//
//        }
//       executor.shutdown();

        for (int i = 0; i < 10 ; i++) {
            ThreadConuter threadConuter = new ThreadConuter();
            threadConuter.setName("thread"+i);
            threadConuter.start();
        }

        Thread.sleep(150000);
        System.out.println("conter = "+counter);
    }

    static class ThreadConuter extends Thread{

        @Override
        public void run() {
            for (int i=0;i<1000000;i++) {
                ThreadUnsafeConuter.counter =  ThreadUnsafeConuter.counter+1;
            }
            System.out.println("当前线程名称："+this.getName()+" 当前counter:"+ThreadUnsafeConuter.counter);
        }
    }

}
