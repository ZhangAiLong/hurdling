package com.zhangal.hurding.basic.hash;

import java.util.HashMap;

public class HashMapTest {

    public static void main(String[] args) {
      HashMap<String,Integer> map = new HashMap<String,Integer>();
        Integer a = map.put("a", 1000);
        System.out.println("a.value="+a);
        Integer b = map.put("b", 1001);
        System.out.println("b.value="+b);

//      map.put("c",1000);
//      map.put("d",1000);
        a = map.put("a",1002);
        System.out.println("a.value="+a);
        for (int i=0;i<100;i++) {
            Integer put = map.put("" + i, i);
            System.out.println(put);

        }

        map.put(null,null);
        map.put("null",null);

        System.out.println(1 << 30);
    }
}
