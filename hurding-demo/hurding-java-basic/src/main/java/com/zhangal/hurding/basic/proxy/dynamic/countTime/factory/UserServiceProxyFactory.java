package com.zhangal.hurding.basic.proxy.dynamic.countTime.factory;

import com.zhangal.hurding.basic.proxy.dynamic.countTime.proxy.UserServiceProxy;

public class UserServiceProxyFactory implements IUserServiceProxyFactory {

   public static UserServiceProxyFactory userServiceProxyFactory = new UserServiceProxyFactory();

   public static UserServiceProxyFactory getInstance() {
       return userServiceProxyFactory;
   }

    @Override
    public UserServiceProxy createUserServiceProxy() {
        return new UserServiceProxy();
    }
}
