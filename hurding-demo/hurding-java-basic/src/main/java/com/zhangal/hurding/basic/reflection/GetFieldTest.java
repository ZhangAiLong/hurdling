package com.zhangal.hurding.basic.reflection;

import java.lang.reflect.Field;

class Parent {
    private int parentField;

    public Parent(int parentField) {
        this.parentField = parentField;
    }

    public void printParentField() {
        System.out.println("Parent field value: " + parentField);
    }
}

class Child extends Parent {
    private String childField;

    public Child(int parentField, String childField) {
        super(parentField);
        this.childField = childField;
    }

    public void printChildField() {
        System.out.println("Child field value: " + childField);
    }
}


public class GetFieldTest {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Parent child = new Child(10, "Hello");

        Class<?> childClass = child.getClass();
        Field childField = childClass.getDeclaredField("childField2");
        childField.setAccessible(true); // 设置为可访问

        Object value = childField.get(child);
        System.out.println("Child field value: " + value);
    }
}
