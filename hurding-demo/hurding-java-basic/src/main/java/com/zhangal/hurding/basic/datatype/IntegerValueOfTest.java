package com.zhangal.hurding.basic.datatype;

public class IntegerValueOfTest {

    public static void main(String[] args) {
        Integer a = 2;
        Integer b = new Integer(2);

        System.out.println(a==b);


        Long la = 3L;
        Long ab = 3L;
        System.out.println(la==ab);

        Integer i = Integer.valueOf(128);
        Integer j = Integer.valueOf(128);
        System.out.println(i==j);
    }
}
