package com.zhangal.hurding.basic.iodemo.bio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TimeMultithreadingClient {

    public static void main(String[] args) {
        int port = 8181;
        Socket  socket = null;
        ExecutorService executor = Executors.newFixedThreadPool(100);
        for (int i = 0; i <100000 ; i++) {
            try {
                socket = new Socket("127.0.0.1",port);
                executor.submit(new TimeClentTask(socket,"TimeClentTask"+i));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        executor.shutdown();
    }

    static  class TimeClentTask  implements Runnable  {
        private Socket socket;
        private String threadName;

        public TimeClentTask(Socket socket,String threadName) {
            this.socket = socket;
            this.threadName = threadName;

        }


        @Override
        public void run() {
            BufferedReader in = null;
            PrintWriter out  = null;

            try {

                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(),true);
                out.println("QUERY TIME ORDER");
                System.out.println("Send Order 2 server succeed.");
                String s = in.readLine();
                System.out.println(threadName +" get Now is: "+s);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (out !=null) {
                    out.close();
                }
                if (in !=null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (socket!=null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

}
