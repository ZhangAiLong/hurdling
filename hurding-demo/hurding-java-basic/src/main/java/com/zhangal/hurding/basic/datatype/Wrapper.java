package com.zhangal.hurding.basic.datatype;

/**
 *封包解包的过程
 */
public class Wrapper {

    public static void main(String[] args) {
        int i = 500;
        Integer t = new Integer(i);
        int j = t.intValue();
        String s = t.toString();
        System.out.println(t);
        Integer t1 = new Integer(500);
        System.out.println(t.equals(t1));
        Integer a1 = 5;
        int a2 = new Integer(5);
        System.out.println("a1 = " + a1 + ",a2 = " + a2);
    }
}
