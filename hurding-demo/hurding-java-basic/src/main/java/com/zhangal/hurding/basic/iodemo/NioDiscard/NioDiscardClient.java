package com.zhangal.hurding.basic.iodemo.NioDiscard;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName NioDiscardClient
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/3 19:55
 **/
public class NioDiscardClient {
   public static void startClient() throws IOException {
       InetSocketAddress address = new InetSocketAddress("127.0.0.1",12345);
       //1、获取通道
       SocketChannel socketChannel = SocketChannel.open(address);
       //2、切换成非阻塞模式
       socketChannel.configureBlocking(false);
       //3、不断的自旋、等待连接完成，或者做一些其他事情
       while (!socketChannel.finishConnect()) {
           //不断的重连中
       }
       System.out.println("客户端连接成功");
       //4、分配指定大小的缓存区
       ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
       byteBuffer.put("hello world".getBytes(StandardCharsets.UTF_8));
       byteBuffer.flip();
       //5、发送到服务器
       socketChannel.write(byteBuffer);
       socketChannel.shutdownOutput();
       socketChannel.close();
       System.out.println("执行完毕。。。。。");

   }

    public static void main(String[] args) throws IOException {
        startClient();
    }
}
