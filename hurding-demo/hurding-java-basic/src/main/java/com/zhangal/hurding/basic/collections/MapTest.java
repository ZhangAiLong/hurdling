package com.zhangal.hurding.basic.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        Map<String, List<String>> testMap = new HashMap<>();
        testMap.computeIfAbsent("10", a -> new ArrayList<>()).add("11111");
        System.out.println(testMap);
        testMap.computeIfAbsent("10", a -> new ArrayList<>()).add("22222");
        System.out.println(testMap);

        for (int i = 0; i < 1000000; i++) {
            System.out.println("======");

        }
    }
}
