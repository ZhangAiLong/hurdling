package com.zhangal.hurding.common.utils.tree.data;

import java.util.ArrayList;
import java.util.List;

public  class TreeNode {
    private int parentId;
    private int nodeId;
    private String label;
    private List<TreeNode> children = new ArrayList<>();
    private String sortIndex="0";

    public TreeNode(int nodeId,String label,int parentId) {
        this.parentId = parentId;
        this.nodeId = nodeId;
        this.children = new ArrayList<>();
        this.label = label;
    }

    public int getParentId() {
        return parentId;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void addChild(TreeNode child) {
        children.add(child);
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public String getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(String sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean hasChildren() {
        if (children!=null && !children.isEmpty())
            return true;
        else
            return false;
    }

    public void removeChild(TreeNode child) {
        if (children != null) {
            children.remove(child);
        }
    }
}





