package com.zhangal.hurding.common.utils.tree.data;

public class ThirdLevelNode extends TreeNode {
    boolean thirdLevelField;
    private String type;
    public ThirdLevelNode(int id,  String label,int parentId,boolean thirdLevelField) {
        super(id, label,parentId);
        this.thirdLevelField = thirdLevelField;
    }
    public ThirdLevelNode(int id,  String label,int parentId, String type,boolean thirdLevelField) {
        super(id, label,parentId);
        this.thirdLevelField = thirdLevelField;
        this.type = type;
    }

    public boolean getThirdLevelField() {
        return thirdLevelField;
    }

    public void setThirdLevelField(boolean thirdLevelField) {
        this.thirdLevelField = thirdLevelField;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
