package com.zhangal.hurding.common.utils.tree.data;

public class SecondLevelNode extends TreeNode {
    int secondLevelField;
    private String type;


    public SecondLevelNode(int id, String label,int parentId, int secondLevelField) {
        super(id, label,parentId);
        this.secondLevelField = secondLevelField;
    }
    public SecondLevelNode(int id, String label,int parentId, String type ,int secondLevelField) {
        super(id, label,parentId);
        this.secondLevelField = secondLevelField;
        this.type= type;
    }

    public int getSecondLevelField() {
        return secondLevelField;
    }

    public void setSecondLevelField(int secondLevelField) {
        this.secondLevelField = secondLevelField;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}