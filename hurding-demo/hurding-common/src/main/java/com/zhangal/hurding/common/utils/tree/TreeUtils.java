package com.zhangal.hurding.common.utils.tree;


import com.zhangal.hurding.common.utils.tree.data.FirstLevelNode;
import com.zhangal.hurding.common.utils.tree.data.SecondLevelNode;
import com.zhangal.hurding.common.utils.tree.data.ThirdLevelNode;
import com.zhangal.hurding.common.utils.tree.data.TreeNode;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class TreeUtils {
    private Map<Integer, TreeNode> nodeMap = new HashMap<>();
    private ReentrantLock lock = new ReentrantLock();

    public  List<TreeNode> buildTrees(List<TreeNode> nodeList) {
        lock.lock();
        try {
            nodeMap.clear();

            // Create nodes and put them into the map
            for (TreeNode node : nodeList) {
                nodeMap.put(node.getNodeId(), node);
            }

            List<TreeNode> roots = new ArrayList<>();

            // Build tree structure by connecting parent and child nodes
            for (TreeNode node : nodeList) {
                if (node.getParentId() == 0) {
                    // Found root node
                    roots.add(node);
                } else {
                    TreeNode parent = nodeMap.get(node.getParentId());
                    if (parent != null) {
                        parent.getChildren().add(node);
                    }
                }
            }

            return roots;
        } finally {
            lock.unlock();
        }
    }


    public static void removeNodesByType(List<TreeNode> roots,  String fieldName,String fieldValue) {
        if (roots == null || roots.isEmpty()) {
            return;
        }
        for (int i = roots.size() - 1; i >= 0; i--) {
            TreeNode root = roots.get(i);
            removeMatchingNodes(root, fieldName, fieldValue,null);
            Object v = getFiledValue(root,root.getClass(),fieldName);

            if (fieldValue.equals(v) && !root.hasChildren()) {
                roots.remove(i);
            }
        }
    }

    private static void removeMatchingNodes(TreeNode node,  String fieldName,String fieldValue,TreeNode parent) {
        Object v = getFiledValue(node,node.getClass(),fieldName);

        if (fieldValue.equals(v) && !node.hasChildren()) {
            parent.removeChild(node); // 从父节点的子节点列表中删除
        } else {
            List<TreeNode> children = new ArrayList<>(node.getChildren()); // 复制子节点列表
            for (TreeNode child : children) {
                removeMatchingNodes(child, fieldName,fieldValue, node);
                Object v2 = getFiledValue(child,child.getClass(),fieldName);
                if (fieldValue.equals(v2) && !child.hasChildren()) {
                    node.removeChild(child); // 在原列表中删除子节点
                }
            }

        }
    }

    private static Object getFiledValue(Object obj,Class clasz,String fieldName) {
        try {
            Field childField = clasz.getDeclaredField(fieldName);
            childField.setAccessible(true); // 设置为可访问
           return childField.get(obj);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }



    private static TreeNode findParent(TreeNode node, int target) {
        if (node == null) {
            return null;
        }

        List<TreeNode> children = node.getChildren();
        for (TreeNode child : children) {
            if (child.getNodeId() == target) {
                return node;
            } else {
                TreeNode parent = findParent(child, target);
                if (parent != null) {
                    return parent;
                }
            }
        }

        return null;
    }


    public static List<TreeNode> filterTree(List<TreeNode> roots, String keyword) {
        List<TreeNode> filteredNodes = new ArrayList<>();
        for (TreeNode root : roots) {
            boolean rootHasKeyword = filterNode(root, keyword, filteredNodes);
            if (rootHasKeyword) {
                filteredNodes.add(root); // 如果根节点包含关键字，则直接添加到结果列表中
            }
        }
        return filteredNodes;
    }

    private static boolean filterNode(TreeNode node, String keyword,  List<TreeNode> filteredNodes) {
        boolean hasKeyword = false;
        if (node.getLabel()!=null && node.getLabel().contains(keyword)) { // 如果当前节点包含指定字段，检查是否包含关键字
            hasKeyword = true;
        }
        List<TreeNode> matchedChildren = new ArrayList<>(); // 用于保存包含关键字的子节点
        for (TreeNode child : node.getChildren()) {
            boolean childHasKeyword = filterNode(child, keyword,  matchedChildren);
            if (childHasKeyword) {
                matchedChildren.add(child);
                hasKeyword = true;
            }
        }
        node.getChildren().clear();
        if ( !matchedChildren.isEmpty()) { // 如果当前节点不包含关键字，但有某个子节点匹配成功，将其添加到结果列表中
            node.getChildren().addAll(matchedChildren);
        }
        return hasKeyword;
    }


    public static List<TreeNode> sortRoots(List<TreeNode> roots) {
        List<TreeNode> sortedRoots = new ArrayList<>(roots);
        Collections.sort(sortedRoots, Comparator.comparing(TreeNode::getSortIndex));
        for (TreeNode root : sortedRoots) {
            sortTree(root);
        }
        return sortedRoots;
    }

    private static void sortTree(TreeNode root) {
        if (root == null) {
            return;
        }

        Collections.sort(root.getChildren(), Comparator.comparing(TreeNode::getSortIndex));
        for (TreeNode child : root.getChildren()) {
            sortTree(child);
        }
    }

    // Example usage
    public static void main(String[] args) {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(new FirstLevelNode(1, "默认节点",0, "node","First Level 1"));
        nodeList.add(new FirstLevelNode(2, "北京节点", 1,"node","First Level 2"));
        nodeList.add(new FirstLevelNode(20, "柳州节点", 2,"node","First Level 2"));
        nodeList.add(new FirstLevelNode(21, "儋州州节点", 20,"node","First Level 2"));
        nodeList.add(new SecondLevelNode(3, "达迪310",1, "device",123));
       // nodeList.add(new ThirdLevelNode(4, "板块1",3, "borad",true));
       // nodeList.add(new ThirdLevelNode(5, "板块12",3, false));
        nodeList.add(new SecondLevelNode(6, "DD190",21, "device",456));
       // nodeList.add(new ThirdLevelNode(7, "设备板卡",6, true));

        TreeUtils treeUtils = new TreeUtils();
        List<TreeNode> roots = treeUtils.buildTrees(nodeList);
        roots = sortRoots(roots);
       // roots =  filterTree(roots,"节点");
        TreeUtils.removeNodesByType(roots, "type","node");
        //TreeUtils.removeNodesByType(roots, "type","node");
        for (TreeNode root : roots) {
            treeUtils.printTree(root, 0);
        }
    }
    public void printTree(TreeNode root, int level) {
        if (root == null) {
            return;
        }

        StringBuilder indent = new StringBuilder();
        for (int i = 0; i < level; i++) {
            indent.append("  ");
        }

//        System.out.println(indent + "- "+root.getLabel()+"Node ID: " + root.getNodeId());

        if (root instanceof FirstLevelNode) {
            FirstLevelNode firstLevelNode = (FirstLevelNode) root;
            System.out.println(indent + "  - "+firstLevelNode.getLabel()+" First Level Field: " + firstLevelNode.getFirstLevelField());
        } else if (root instanceof SecondLevelNode) {
            SecondLevelNode secondLevelNode = (SecondLevelNode) root;
            System.out.println(indent + "  - "+secondLevelNode.getLabel()+" Second Level Field: " + secondLevelNode.getSecondLevelField());

        } else if (root instanceof ThirdLevelNode) {
            ThirdLevelNode thirdLevelNode = (ThirdLevelNode) root;
            System.out.println(indent + "  - "+thirdLevelNode.getLabel()+"  Third Level Field: " + thirdLevelNode.getThirdLevelField());
        }

        for (TreeNode child : root.getChildren()) {
            printTree(child, level + 1);
        }
    }

}
