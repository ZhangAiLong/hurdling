package com.zhangal.hurding.common.utils.tree.data;

public class FirstLevelNode extends TreeNode {
    String firstLevelField;
    private String type;

    public FirstLevelNode(int id, String label,int parentId, String firstLevelField) {
        super(id, label,parentId);
        this.firstLevelField = firstLevelField;
    }

    public FirstLevelNode(int id, String label,int parentId, String type,String firstLevelField) {
        super(id, label,parentId);
        this.firstLevelField = firstLevelField;
        this.type= type;

    }
    public String getFirstLevelField() {
        return firstLevelField;
    }

    public void setFirstLevelField(String firstLevelField) {
        this.firstLevelField = firstLevelField;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
