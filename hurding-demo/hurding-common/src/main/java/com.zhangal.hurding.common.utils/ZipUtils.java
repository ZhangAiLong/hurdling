package com.zhangal.hurding.common.utils;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * Created by luorh on 2017/5/26.
 */
public class ZipUtils {
    public static void zip(String sourceFileName, String destFileName) throws IOException {
        File sourceFile = new File(sourceFileName);
        File zipFile = new File(destFileName);
        try(FileOutputStream outputStream = new FileOutputStream(zipFile);
            ZipArchiveOutputStream zipOutputStream = new ZipArchiveOutputStream(outputStream)) {
            addZipEntry("/", sourceFile, zipOutputStream);
        }
    }

    private static void addZipEntry(String basePath, File sourceFile, ZipArchiveOutputStream zipOutputStream) throws IOException {
        String entryName = basePath + sourceFile.getName();
        if (sourceFile.isDirectory()) {
            File[] children = sourceFile.listFiles();
            if (children == null)
                return;
            for (File file: children) {
                addZipEntry(entryName + "/", file, zipOutputStream);
            }
        } else {
            try (FileInputStream inputStream = new FileInputStream(sourceFile);
                 BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)) {
                ZipArchiveEntry zipEntry = new ZipArchiveEntry(entryName);
                zipOutputStream.putArchiveEntry(zipEntry);
                IOUtils.copy(bufferedInputStream, zipOutputStream);
                zipOutputStream.closeArchiveEntry();
            }
        }
    }

    /**
     * 解压到同级目录，返回目录路径
     * @param zipFileName
     * @return
     * @throws IOException
     */
    public static String unzip(String zipFileName) throws IOException {
        String fileDir = FilenameUtils.getFullPath(zipFileName);
        String baseName = FilenameUtils.getBaseName(zipFileName);
        String destFilePath = FilenameUtils.concat(fileDir, baseName);
        unzip(zipFileName, destFilePath);
        return destFilePath;
    }

    public static void unzip(String zipFileName, String destFilePath) throws IOException {
        unzip(zipFileName, destFilePath, "UTF8", true, true);
    }

    public static void unzip(
            String zipFileName, String destFilePath, String encoding,
            boolean useUnicodeExtraFields, boolean allowStoredEntriesWithDataDescriptor) throws IOException {
        File zipFile = new File(zipFileName);
        try(FileInputStream inputStream = new FileInputStream(zipFile);
            ZipArchiveInputStream zipInputStream = new ZipArchiveInputStream(
                    inputStream,
                    encoding,
                    useUnicodeExtraFields,
                    allowStoredEntriesWithDataDescriptor)) {
            saveZipEntry(destFilePath, zipInputStream);
        }
    }

    private static void saveZipEntry(String basePath, ZipArchiveInputStream zipInputStream) throws IOException {
        ZipArchiveEntry entry;
        while((entry = zipInputStream.getNextZipEntry()) != null) {
            if(!entry.isDirectory()){
                File target = new File(basePath, entry.getName());
                if (!target.getParentFile().exists()) {
                    target.getParentFile().mkdirs();
                }
                try (FileOutputStream outputStream = new FileOutputStream(target);
                     BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream)) {
                    IOUtils.copy(zipInputStream, bufferedOutputStream);
                    bufferedOutputStream.flush();
                }
            }else{
                String name = entry.getName();
                name = name.substring(0, name.length() - 1);
                File file = new File(basePath + File.separator + name);
                file.mkdir();
            }

        }
    }
}
