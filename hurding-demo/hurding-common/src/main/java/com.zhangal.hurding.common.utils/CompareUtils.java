package com.zhangal.hurding.common.utils;

/**
 * Created by luorh on 2017/3/7.
 */
public class CompareUtils {
    public static boolean equals(Object source, Object target) {
        if (source == null) {
            return target == null;
        } else {
            if (target == null)
                return false;
            if (!source.getClass().equals(target.getClass()))
                return false;
            return source.equals(target);
        }
    }
}
