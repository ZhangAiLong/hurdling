package com.zhangal.hurding.common.utils;

import java.lang.reflect.Field;

/**
 * Created by luorh on 2018/1/2.
 */
public class BeanExtUtils {
    public static <T> void mergeObject(T origin, T destination) {
        if (origin == null || destination == null)
            return;
        if (!origin.getClass().equals(destination.getClass()))
            throw new IllegalArgumentException("can not merge different type objects");

        Field[] fields = origin.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object value = field.get(origin);
                if (null != value) {
                    field.set(destination, value);
                }
                field.setAccessible(false);
            } catch (Exception ignored) {
            }
        }
    }
}
