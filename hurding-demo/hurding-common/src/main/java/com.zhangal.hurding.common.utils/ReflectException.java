package com.zhangal.hurding.common.utils;

/**
 * Created by luorh on 2016/10/25.
 */
public class ReflectException extends RuntimeException {
    public ReflectException(String message) {
        super(message);
    }

    public ReflectException(String message, Throwable cause) {
        super(message, cause);
    }
}
