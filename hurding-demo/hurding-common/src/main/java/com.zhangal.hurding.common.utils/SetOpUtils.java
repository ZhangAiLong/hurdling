package com.zhangal.hurding.common.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by luorh on 2017/4/25.
 */
public class SetOpUtils {
    public static <T> Set<T> intersect(Set<T> dest, Set<T> src) {
        Set<T> set = new HashSet<T>(src.size());
        copy(set, src);
        set.retainAll(dest);
        return set;
    }

    public static <T> boolean hasIntersection(Set<T> dest, Set<T> src) {
        Set<T> intersection = intersect(dest, src);
        return !intersection.isEmpty();
}

    public static <T> Set<T> union(Set<T> dest, Set<T> src) {
        Set<T> set = new HashSet<T>(src.size());
        copy(set, src);
        set.addAll(dest);
        return set;
    }

    public static <T> Set<T> diff(Set<T> dest, Set<T> src) {
        Set<T> set = new HashSet<T>(src.size());
        copy(set, src);
        set.removeAll(dest);
        return set;
    }

    private static <T> void copy(Set<T> dest, Set<T> src) {
        dest.addAll(src);
    }
}
