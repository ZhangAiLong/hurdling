package com.zhangal.hurding.common.utils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.UUID;

/**
 * Created by luorh on 2017/5/26.
 */
public class FileUtils {
    public static String getTempDir() {
        return System.getProperty("java.io.tmpdir");
    }

    public static String getTempFileName(String fileExtension) {
        String fileName = UUID.randomUUID().toString() + '.' + fileExtension;
        return FilenameUtils.concat(getTempDir(), fileName);
    }

    public static boolean deleteFile(String filePath) {
        File file = new File(filePath);
        return file.exists() && file.delete();
    }
}
