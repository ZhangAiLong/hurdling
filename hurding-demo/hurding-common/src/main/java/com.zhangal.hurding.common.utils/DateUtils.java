package com.zhangal.hurding.common.utils;

import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by guoj-h on 2017/6/13.
 */
public class DateUtils {

    private static final String PATTERN_DATE_GENERAL = "yyyy-MM-dd";

    private static final String PATTERN_DATETIME_GENERAL = "yyyy-MM-dd HH:mm:ss";

    private static final String PATTERN_TIME_GENERAL = "HH:mm:ss";

    private static final String PATTERN_MONTH_GENERAL = "yyyy-MM";

    private static final String PATTERN_MONTH_WITHOUT_SEPARATOR = "yyyyMM";

    //获取周第一天
    public static Date getStartDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
        return cal.getTime();
    }

    public static java.sql.Date getStartDayOfWeek(java.sql.Date date) {
        return new java.sql.Date(DateUtils.getStartDayOfWeek(new Date(date.getTime())).getTime());
    }

    //获取周最后一天
    public static Date getEndDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
        return cal.getTime();
    }

    public static java.sql.Date getEndDayOfWeek(java.sql.Date date) {
        return new java.sql.Date(DateUtils.getEndDayOfWeek(new Date(date.getTime())).getTime());
    }

    //获取月第一天
    public static Date getStartDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DATE, cal.getActualMinimum(Calendar.DATE));
        return cal.getTime();
    }

    public static java.sql.Date getStartDayOfMonth(java.sql.Date date) {
        return new java.sql.Date(DateUtils.getStartDayOfMonth(new Date(date.getTime())).getTime());
    }

    //获取月最后一天
    public static Date getEndDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        return cal.getTime();
    }

    public static java.sql.Date getEndDayOfMonth(java.sql.Date date) {
        return new java.sql.Date(DateUtils.getEndDayOfMonth(new Date(date.getTime())).getTime());
    }

    /**
     * 获取间隔天数（忽略时分秒）
     * @param start 开始时间
     * @param end 结束时间
     * @throws Exception
     * @return
     */
    public static int getIntervalDaysNumIgnoreTime(Date start, Date end) {
        Assert.notNull(start, "参数start[开始时间]不能为空");
        Assert.notNull(end, "参数end[结束时间]不能为空");
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE_GENERAL);
            start = sdf.parse(sdf.format(start));
            end = sdf.parse(sdf.format(end));
        } catch (ParseException e) {
            // 固定格式下此处不会发生异常
        }
        int num = getIntervalDaysNum(start, end);
        return num + 1;
    }

    /**
     * 获取间隔天数（精确到时分秒）
     * @param start 开始时间
     * @param end 结束时间
     * @return
     */
    public static int getIntervalDaysNum(Date start, Date end) {
        Assert.notNull(start, "参数start[开始时间]不能为空");
        Assert.notNull(end, "参数end[结束时间]不能为空");
        if (start.compareTo(end) > 0) {
            throw new IllegalArgumentException("参数start[开始时间]不能大于end[结束时间]");
        }
        long startTimeMillis = start.getTime();
        long endTimeMillis = end.getTime();
        int num = (int)((endTimeMillis - startTimeMillis) / (1000 * 60 * 60 *24));
        return num;
    }

    /**
     * 获取当前日期（时分秒清零，格式为[yyyy-MM-dd 00:00:00]）
     * @return
     */
    public static Date getCurrentDate() {
        Date date = new Date();
        return getDateWithoutTime(date);
    }

    /**
     * 获取日期（时分秒清零，格式为[yyyy-MM-dd 00:00:00]）
     * @param date
     * @return
     */
    public static Date getDateWithoutTime(Date date) {
        Assert.notNull(date, "参数date不能为空");
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE_GENERAL);
        try {
            date = sdf.parse(sdf.format(date));
        } catch (ParseException e) {
            // 固定格式下此处不会发生异常
        }
        return date;
    }

    /**
     * 根据间隔天数获取新日期（interval为正数时往后加，负数时往前减）
     * @param date
     * @param interval 间隔天数
     * @return
     */
    public static Date getDateByIntervalDays(Date date, int interval) {
        Assert.notNull(date, "参数date不能为空");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, interval);
        return cal.getTime();
    }

    /**
     * 根据间隔月数获取新日期（interval为正数时往后加，负数时往前减）
     * @param date
     * @param interval 间隔月数
     * @return
     */
    public static Date getDateByIntervalMonths(Date date, int interval) {
        Assert.notNull(date, "参数date不能为空");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, interval);
        return cal.getTime();
    }

    /**
     * 获取日期对应的年份
     * @param date
     * @return
     */
    public static int getYear(Date date) {
        Assert.notNull(date, "参数date不能为空");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    /**
     * 获取日期对应年份的星期数
     * @param date
     * @return
     */
    public static int getWeek(Date date) {
        Assert.notNull(date, "参数date不能为空");
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * 将date转为yyyyMM格式字符串
     * @param date
     * @return
     */
    public static String formatDateToMonthWithoutSeparator(Date date) {
        Assert.notNull(date, "参数date不能为空");
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_MONTH_WITHOUT_SEPARATOR);
        return sdf.format(date);
    }

    /**
     * 将util.Date格式化为yyyy-MM-dd 00:00:00格式
     * @param date
     * @return
     */
    public static Date formatDateTimeWithoutTime(Date date) {
        Date result = null;
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE_GENERAL);
        try {
            result = sdf.parse(sdf.format(date));
        } catch (ParseException e) {
            // 固定格式下此处不会发生异常
        }
        return result;
    }

    /*public static void main(String[] args) throws ParseException {
        Date a = new Date();
        Date b = java.sql.Date.valueOf("2017-6-25");
        long s = System.currentTimeMillis();
        System.out.println(getIntervalDaysNum(a, b));
        System.out.println(getIntervalDaysNumIgnoreTime(a, b));
        long e = System.currentTimeMillis();
        System.out.println(e-s);
        System.out.println(getCurrentDate());
    }*/
}
