package com.zhangal.hurding.common.utils;

/**
 * Created by luorh on 2017/2/8.
 */
public class ExceptionUtils {
    public Throwable getOriginalCause(Exception ex) {
        Throwable result = ex;
        while (result.getCause() != null) {
            result = result.getCause();
        }
        return result;
    }
}
