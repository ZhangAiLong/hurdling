package com.zhangal.hurding.common.utils;

/**
 * Created by luorh on 2017/7/20.
 */
public class RetryException extends RuntimeException {
    public RetryException(String message) {
        super(message);
    }

    public RetryException(String message, Throwable cause) {
        super(message, cause);
    }
}
