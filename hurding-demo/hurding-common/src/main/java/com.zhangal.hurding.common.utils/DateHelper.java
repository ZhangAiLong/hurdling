
package com.zhangal.hurding.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Date Utility Class. And please use Apache common DateUtils to add, judge equality, round,
 * truncate date.
 *
 * @author sherlockq
 */
public class DateHelper {

    private static Logger logger = LoggerFactory.getLogger(DateHelper.class);

    /**
     * DateFormat cache map
     */
    protected static Map<String, DateFormat> dateFormatMap = new HashMap<String, DateFormat>();

    /**
     * Date format pattern
     */
    public final static String FORMAT_DATE_DEFAULT = "yyyy-MM-dd";
    public final static String FORMAT_DATE_YYYYMMDD = "yyyyMMdd";
    public final static String FORMAT_DATE_YYYY_MM_DD = "yyyy-MM-dd";
    public final static String FORMAT_DATE_SLASH_YYYY_MM_DD = "yyyy/MM/dd";
    public final static String FORMAT_DATE_SLASH_YYYY_M_DD = "yyyy/M/dd";
    public final static String FORMAT_DATE_SLASH_YYYY_MM_D = "yyyy/MM/d";
    public final static String FORMAT_DATE_SLASH_YYYY_M_D = "yyyy/M/d";

    public static void main(String[] args) {
        Date now = new Date();
        // System.out.println(formatDate(now, FORMAT_DATETIME_YYYYMMDD_HHMMSS));
        Date date1 = new Date();
        Date date2 = new Date();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date2);
        instance.set(Calendar.DATE,18);
        date2 = instance.getTime();
        boolean isSame = false;
        isSame =  DateHelper.compareDateToField(date1,date2,Calendar.YEAR);
//        System.out.println("YEAR:"+isSame);
//        isSame =  DateHelper.compareDateToField(date1,date2,Calendar.MONTH);
//        System.out.println("MONTH:"+isSame);
//        isSame =  DateHelper.compareDateToField(date1,date2,Calendar.HOUR);
//        System.out.println("HOUR:"+isSame);
//        isSame =  DateHelper.compareDateToField(date1,date2,Calendar.MINUTE);
//        System.out.println("MINUTE:"+isSame);
//        isSame =  DateHelper.compareDateToField(date1,date2,Calendar.SECOND);
//        System.out.println("SECOND:"+isSame);
        date1 = DateHelper.parse("2020-10-01","yyyy-MM-dd");
        date2 = DateHelper.parse("2020-10-01","yyyy-MM-dd");
//
        Date[] intervalDateArrays = DateHelper.getShardingDateArrays(date1, date2, Calendar.MINUTE,3);
//        for (int i=0;i<intervalDateArrays.length;i++) {
//           Date date =  intervalDateArrays[i];
//            System.out.println( DateHelper.formatDate(date,FORMAT_DATETIME_YYYY_MM_DD_HH_MM));
//
//        }
        System.out.println( DateHelper.formatDate(intervalDateArrays[0],FORMAT_DATETIME_YYYY_MM_DD_HH_MM));
        System.out.println( DateHelper.formatDate(intervalDateArrays[intervalDateArrays.length-1],FORMAT_DATETIME_YYYY_MM_DD_HH_MM));

//         intervalDateArrays = DateHelper.getShardingDateArrays(date1, date2, Calendar.DATE,1);
//        System.out.println( DateHelper.formatDate(intervalDateArrays[0],FORMAT_DATETIME_YYYY_MM_DD_HH_MM));
//        System.out.println( DateHelper.formatDate(intervalDateArrays[intervalDateArrays.length-1],FORMAT_DATETIME_YYYY_MM_DD_HH_MM));

//        for (int i=0;i<intervalDateArrays.length;i++) {
//            Date date =  intervalDateArrays[i];
//            System.out.println( DateHelper.formatDate(date,FORMAT_DATETIME_YYYY_MM_DD_HH_MM));
//
//        }
        Date date = DateHelper.setQueryStartTime(new Date(), Calendar.DATE);
        System.out.println( DateHelper.formatDate(date,FORMAT_DATETIME_YYYY_MM_DD_HH_MM_SS_SSS));

    }

    /**
     * DateTime format pattern
     */
    public final static String FORMAT_DATETIME_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_DATETIME_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_DATETIME_YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
    public final static String FORMAT_DATETIME_YYYY_MM_DD_HHMM = "yyyy-MM-dd HHmm";
    public final static String FORMAT_DATETIME_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public final static String FORMAT_DATETIME_YYYY_MM_DD_HHMMSS = "yyyy-MM-dd HHmmss";
    public final static String FORMAT_DATETIME_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public final static String FORMAT_DATETIME_YYYYMMDD_HHMMSS = "yyyy/MM/dd HH:mm:ss";
    public final static String FORMAT_DATETIME_YYYY_MM_DD_HH = "yyyy-MM-dd HH";

    /**
     * Time format pattern
     */
    public final static String FORMAT_TIME_DEFAULT = "HH:mm:ss";
    public final static String FORMAT_TIME_HH_MM = "HH:mm";
    public final static String FORMAT_TIME_HHMM = "HHmm";
    public final static String FORMAT_TIME_HH_MM_SS = "HH:mm:ss";
    public final static String FORMAT_TIME_HHMMSS = "HHmmss";

    /**
     * Returns DateFormat according to given pattern which is cached.<br/>
     * This method is not public due to that the DateFormat instance may be altered by outside.
     *
     * @param formatPattern date format pattern string
     * @return Cached DateFormat instance which should not be altered in any way.
     */
    protected static DateFormat getCachedDateFormat(String formatPattern) {
        DateFormat dateFormat = dateFormatMap.get(formatPattern);

        if (dateFormat == null) {

            dateFormat = new SimpleDateFormat(formatPattern);
            dateFormatMap.put(formatPattern, dateFormat);

        }

        return dateFormat;

    }

    /**
     * Returns DateFormat according to given pattern.
     *
     * @param formatPattern date format pattern string
     * @return DateFormat instance.
     */
    public static DateFormat getDateFormat(String formatPattern) {

        return new SimpleDateFormat(formatPattern);

    }

    /**
     * Format the date according to the pattern.
     *
     * @param date Date to format. If it's null, the result will be null.
     * @param formatPattern Date format pattern string. You can find common ones in DateUtils class.
     * @return Formatted date in String
     * @see DateHelper
     */
    public static String formatDate(Date date, String formatPattern) {
        if (date == null) {
            return null;
        }
        return getCachedDateFormat(formatPattern).format(date);
    }

    /**
     * Format the date in default pattern: yyyy-MM-dd.
     *
     * @param date Date to format. If it's null, the result will be null.
     * @return Formatted date in String
     * @see DateHelper#FORMAT_DATE_DEFAULT
     * @see DateHelper#formatDate(Date, String)
     */
    public static String formatDate(Date date) {
        return formatDate(date, FORMAT_DATE_DEFAULT);
    }

    /**
     * Format the date in default date-time pattern: yyyy-MM-dd HH:mm:ss
     *
     * @param date Date to format. If it's null, the result will be null.
     * @return Formatted date-time in String
     * @see DateHelper#FORMAT_DATETIME_DEFAULT
     * @see DateHelper#formatDate(Date, String)
     */
    public static String formatDateTime(Date date) {
        return formatDate(date, FORMAT_DATETIME_DEFAULT);
    }

    /**
     * Format the date in default time pattern: HH:mm:ss
     *
     * @param date Date to format. If it's null, the result will be null.
     * @return Formatted time in String
     * @see DateHelper#FORMAT_TIME_DEFAULT
     * @see DateHelper#formatDate(Date, String)
     */
    public static String formatTime(Date date) {
        return formatDate(date, FORMAT_TIME_DEFAULT);
    }

    /**
     * Returns current system date.
     *
     * @return current system date.
     * @see Date#Date()
     */
    public static Date getCurrentDate() {
        return new Date();
    }

    /**
     * Parse string value to Date by given format pattern. If parse failed, null would be returned.
     *
     * @param stringValue date value as string.
     * @param formatPattern format pattern.
     * @return Date represents stringValue, null while parse exception occurred.
     */
    public static Date parse(String stringValue, String formatPattern) {
        // logger.info("stringValue:" + stringValue);

        try {
            return getCachedDateFormat(formatPattern).parse(stringValue);
        } catch (ParseException e) {
            logger.debug("fail to parse date.", e);
            return null;
        } catch (Exception e) {

            return null;
        }

    }

    /**
     * Parses given Date to a Timestamp instance with same date in milliseconds precision.
     *
     * @param date Date to parse.
     * @return Timestamp in milliseconds precision
     */
    public static Timestamp parseTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }

    /**
     * Compares two dates based on the specified calendar field, and ignores those fields more
     * trivial. Neither date could be null.<br/>
     * For example, if calendarField is Calendar.Month, then result will be 0 if 2008-8-2 and
     * 2008-8-10 is compared. But the result will be -1 if
     *
     * @param date1
     * @param date2
     * @return date1 < date2 : <0<br/>
     *         date1 = date2 : 0<br/>
     *         date1 > date2 : >0
     * @throws IllegalArgumentException If either date is null.
     * @see Calendar
     */
    public static boolean compareDateToField(final Date date1, final Date date2, int calendarField) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("Neither date could be null");
        }
        // Calendar instance = Calendar.getInstance();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        if (calendarField == Calendar.YEAR) {
            return  cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
        } else if(calendarField == Calendar.MONTH) {
            return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR));
        } else if (calendarField == Calendar.DATE){
            return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
            );
        } else if (calendarField == Calendar.HOUR) {
            return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                    cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY)
            );
        } else if (calendarField == Calendar.MINUTE) {
            return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                    cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY) &&
                    cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE)
            );

        } else  if(calendarField == Calendar.SECOND){
            return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                    cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY) &&
                    cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE) &&
                    cal1.get(Calendar.SECOND) == cal2.get(Calendar.SECOND)
            );

        }
        return  false;
    }

    public static Date getSpecifiedDayBefore(Date now, int beforeDays) {

        Calendar c = Calendar.getInstance();
        c.setTime(now);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day - beforeDays);
        // String dayBefore = new SimpleDateFormat(FORMAT_DATE_DEFAULT).format(c.getTime());
        return c.getTime();
    }

    public static Long dateDiff(Date beginDate, Date endDate, int CALENDAR) {

        long beginTime = parseTimestamp(beginDate).getTime();
        long endTime = parseTimestamp(endDate).getTime();
        long diff = endTime - beginTime;
        if (CALENDAR == Calendar.MINUTE) {
            return diff / (60 * 1000);
        } else if (CALENDAR == Calendar.SECOND) {
            return diff / 1000;
        } else if (CALENDAR == Calendar.HOUR){
            return diff /  (60 * 60 * 1000 );
        } else if(CALENDAR == Calendar.DATE){
            return diff /  (24* 60 * 60 * 1000 );
        }
        return 0L;
    }

    // 获取上月第一天
    public static Date getBeforeFirstMonthDay() {
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MONTH, -1);
        nowTime.set(Calendar.DAY_OF_MONTH, 1);
        return nowTime.getTime();
    }

    // 获取最后一天
    public static Date getMonthLastDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }

    // 获取最后一天
    public static Date getMonthLastSecond(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTime();
    }

    // 获取当月第一天
    public static Date getMonthFirstDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTime();
    }

    //java获取当前月的天数
    public static int getDayOfMonth(Date date){
        Calendar aCalendar = Calendar.getInstance(Locale.CHINA);
        aCalendar.setTime(date);
        int day=aCalendar.getActualMaximum(Calendar.DATE);
        return day;
    }
    /**
     * @Title: gmtTime
     * @Description: (获取当前格林尼治时间)
     * @return void 返回类型
     * @throws @author zhaohy-c
     * @date 2017年7月24日 上午11:04:39
     */
    public static String gmtTime() {
        Calendar cd = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT")); // 设置时区为GMT
        String str = sdf.format(cd.getTime());
        System.out.println(str);
        return str;
    }

    /**
     * 获取当天最后的时间，例子： 2018-08-12 23：59:59
     * @param date
     * @return
     */
    public static Date getDateLastTime(Date date){
        if (date!=null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY,23);
            calendar.set(Calendar.MINUTE,59);
            calendar.set(Calendar.SECOND,59);
            return calendar.getTime();
        }
        return  null;
    }



    /**
     * 获取date字段值
     * @param date
     * @return
     */
    public static Integer getDateField(Date date, int calendarField){
        if (date!=null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if (calendarField==Calendar.MONTH) {
                return calendar.get(calendarField)+1;

            }
            return calendar.get(calendarField);
        }
        return null;
    }

    /**
     * 设置date字段值
     * @param date
     * @param calendarField
     * @param filedValue
     * @return
     */
    public static Date setDateField(Date date, int calendarField,Integer filedValue){
        if (date!=null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(calendarField,filedValue);
            return calendar.getTime();
        }
        return null;
    }


    /**
     * 根据开始时间、结束时间得到两个时间段内所有的日期
     * @param start 开始日期
     * @param end   结束日期
     * @param calendarType  类型
     * @return  两个日期之间的日期
     */
    public static Date[] getIntervalDateArrays(Date start, Date end, int calendarType) {
        ArrayList ret = new ArrayList();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        //calendar.add(calendarType,1);
        Date tmpDate = calendar.getTime();
        long endTime = end.getTime();
        while (tmpDate.before(end) || tmpDate.getTime() == endTime) {

            if(calendarType==Calendar.MINUTE){
                calendar.set(Calendar.SECOND,0);
            }else  if(calendarType==Calendar.HOUR) {
                calendar.set(Calendar.SECOND,0);
                calendar.set(Calendar.MINUTE,0);
            } else if (calendarType==Calendar.DATE){
                calendar.set(Calendar.SECOND,0);
                calendar.set(Calendar.MINUTE,0);
                calendar.set(Calendar.HOUR_OF_DAY,0);
            }
            ret.add(calendar.getTime());
            calendar.add(calendarType, 1);
            tmpDate = calendar.getTime();
        }
        Date[] dates = new Date[ret.size()];
        return (Date[]) ret.toArray(dates);
    }

    /**
     *
     * @param start
     * @param end
     * @param calendarType
     * @param interval
     * @return
     */
    public static Date[] getShardingDateArrays(Date start, Date end, int calendarType,int interval) {
        List<Date> ret = new ArrayList();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(end);
        //calendar.add(calendarType,1);
        Date tmpDate = calendar.getTime();
        long startTime = start.getTime();
        while (tmpDate.after(start)) {
            calendar.set(Calendar.MILLISECOND,0);
            if(calendarType==Calendar.MINUTE){
                calendar.set(Calendar.SECOND,0);
            }else  if(calendarType==Calendar.HOUR) {
                calendar.set(Calendar.SECOND,0);
                calendar.set(Calendar.MINUTE,0);
            } else if (calendarType==Calendar.DATE){
                calendar.set(Calendar.SECOND,0);
                calendar.set(Calendar.MINUTE,0);
                calendar.set(Calendar.HOUR_OF_DAY,0);
            }
            ret.add(calendar.getTime());
            calendar.add(calendarType, -interval);
            tmpDate = calendar.getTime();
        }
        ret.add(tmpDate);
        // 排序
        ret = ret.stream().sorted(Comparator.comparing(Date::getTime)).collect(Collectors.toList());//根据创建时间倒排
        Date[] dates = new Date[ret.size()];
        return (Date[]) ret.toArray(dates);
    }



    public static Date setQueryStartTime(Date startTime,int calendarField ) {
        if (startTime==null) {
            logger.info("startTime is null");
            return  null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        if (calendarField == Calendar.MONTH) {
            calendar.set(Calendar.DAY_OF_MONTH,1);
            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
        }else if (calendarField == Calendar.DATE) {
            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
        }else if (calendarField == Calendar.HOUR_OF_DAY) {
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
        } else if (calendarField == Calendar.MINUTE) {
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
        } else if (calendarField == Calendar.SECOND) {
            calendar.set(Calendar.MILLISECOND,0);
        } else {
            logger.info("setQueryStartTime 无效的 calendarField："+calendarField);
            return null;
        }
        return  calendar.getTime();

    }



    public static Date setQueryEndTime(Date endTime, int calendarField ){
        if (endTime == null) {
            logger.info("date is null");
            return  null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endTime);
        if (calendarField == Calendar.MONTH) {
            calendar.setTime(DateHelper.getMonthLastDay(calendar.getTime()));
            calendar.set(Calendar.HOUR_OF_DAY,23);
            calendar.set(Calendar.MINUTE,59);
            calendar.set(Calendar.SECOND,59);
            calendar.set(Calendar.MILLISECOND,999);
        }else if (calendarField == Calendar.DATE) {
            calendar.set(Calendar.HOUR_OF_DAY,23);
            calendar.set(Calendar.MINUTE,59);
            calendar.set(Calendar.SECOND,59);
            calendar.set(Calendar.MILLISECOND,999);
        }else if (calendarField == Calendar.HOUR_OF_DAY) {
            calendar.set(Calendar.MINUTE,59);
            calendar.set(Calendar.SECOND,59);
            calendar.set(Calendar.MILLISECOND,999);
        } else if (calendarField == Calendar.MINUTE) {
            calendar.set(Calendar.SECOND,59);
            calendar.set(Calendar.MILLISECOND,999);
        } else if (calendarField == Calendar.SECOND) {
            calendar.set(Calendar.MILLISECOND,999);
        } else {
            logger.info("无效的 calendarField："+calendarField);
            return null;
        }
        return  calendar.getTime();
    }

    public  static  String  date2LocalDateTimeToFormat(Date date){
        if (date==null) {
            return  null;
        }
        Instant instant = date.toInstant();//An instantaneous point on the time-line.(时间线上的一个瞬时点。)
        ZoneId zoneId = ZoneId.systemDefault();//A time-zone ID, such as {@code Europe/Paris}.(时区)
        LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();

      //  System.out.println(localDateTime.toString());//2018-03-27T14:07:32.668
        //System.out.println(localDateTime.toLocalDate() + " " +localDateTime.toLocalTime());//2018-03-27 14:48:57.453

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");//This class is immutable and thread-safe.@since 1.8
        String format = dateTimeFormatter.format(localDateTime);
        // System.out.println(dateTimeFormatter.format(localDateTime));//2018-03-27 14:52:57
        return format;
    }

    /**
     * 比较日期同一个Field的值是否相同
     * @param date1
     * @param date2
     * @param calendarField
     * @return
     */
    public static boolean compareFieldIsEqual(Date date1,Date date2,Integer calendarField) {
        if (date1==null || date2==null) {
            return false;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        int v1 = calendar.get(calendarField);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        int v2 = calendar2.get(calendarField);

         return v1 == v2;
    }


}
