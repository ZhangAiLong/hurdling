package com.zhangal.hurding.common.utils;

import java.util.concurrent.Callable;

/**
 * Created by luorh on 2017/7/6.
 */
public class RetryUtils {
    public static void retryExec(Runnable runnable, int times, long retryIntervals) {
        int execTimes = 0;
        try {
            while (execTimes < times) {
                execTimes++;
                try {
                    runnable.run();
                    return;
                } catch (Exception ex) {
                    if (execTimes == times) {
                        throw ex;
                    } else {
                        Thread.sleep(retryIntervals);
                    }
                }
            }
        } catch (Exception ex) {
            throw new RetryException("retry execute error", ex);
        }
    }

    public static <V> V retryExecAndReturn(Callable<V> callable, int times, long retryIntervals) {
        int execTimes = 0;
        try {
            while (execTimes < times) {
                execTimes++;
                try {
                    return callable.call();
                } catch (Exception ex) {
                    if (execTimes == times) {
                        throw ex;
                    } else {
                        Thread.sleep(retryIntervals);
                    }
                }
            }
        } catch (Exception ex) {
            throw new RetryException("retry execute error", ex);
        }
        return null;
    }
}
