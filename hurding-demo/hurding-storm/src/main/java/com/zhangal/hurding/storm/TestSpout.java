package com.zhangal.hurding.storm;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.util.Map;

/**
 * @ClassName HelloRichSpout
 * @Description TODO
 * @Author HQJN
 * @Date 2022/2/16 14:28
 **/
public class TestSpout implements IRichSpout {

    private SpoutOutputCollector collector;

    private int count = 1;
    private static final String field="word";

    private String[] message =  {
            "My nickname is xuwujing",
            "My blog address is http://www.panchengming.com/",
            "My interest is playing games"
    };

    @Override
    public void open(Map<String, Object> map, TopologyContext topologyContext, SpoutOutputCollector collector) {
        System.out.println("open:"+map.get("test"));
        this.collector = collector;
    }

    @Override

    public void close() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }

    @Override
    public void nextTuple() {
        if(count<=message.length){
            System.out.println("第"+count +"次开始发送数据...");
            this.collector.emit(new Values(message[count-1]));
        }
        count++;
    }

    @Override
    public void ack(Object o) {
        System.out.println("ack:"+o);
    }

    @Override
    public void fail(Object o) {
        System.out.println("fail:"+o);

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        System.out.println("定义格式...");
        outputFieldsDeclarer.declare(new Fields(field));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
