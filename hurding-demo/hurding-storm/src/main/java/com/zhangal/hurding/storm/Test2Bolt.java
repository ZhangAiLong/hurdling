package com.zhangal.hurding.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName Test2Bolt
 * @Description TODO
 * @Author HQJN
 * @Date 2022/2/16 18:15
 **/
public class Test2Bolt extends BaseRichBolt {
    private long count=1;


    /**
     * 保存单词和对应的计数
     */
    private HashMap<String, Integer> counts = null;

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        System.out.println("prepare:"+map.get("test"));
        this.counts=new HashMap<String, Integer>();
    }

    @Override
    public void execute(Tuple tuple) {
        String msg=tuple.getStringByField("count");
        System.out.println("第"+count+"次统计单词出现的次数");
        /**
         * 如果不包含该单词，说明在该map是第一次出现
         * 否则进行加1
         */
        if (!counts.containsKey(msg)) {
            counts.put(msg, 1);
        } else {
            counts.put(msg, counts.get(msg)+1);
        }
        count++;
    }


    /**
     * cleanup是IBolt接口中定义,用于释放bolt占用的资源。
     * Storm在终止一个bolt之前会调用这个方法。
     */
    @Override
    public void cleanup() {
        System.out.println("===========开始显示单词数量============");
        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        System.out.println("===========结束============");
        System.out.println("Test2Bolt的资源释放");
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
}
