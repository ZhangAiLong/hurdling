package com.zhangal.hurding.netty.chatdemo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.EventExecutor;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.SimpleFormatter;

public class ChatNettyServerHandler extends SimpleChannelInboundHandler<String> {

    //全局事件执行器，是个单例
    public static ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

/*    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("服务器读取线程:"+Thread.currentThread().getName());
        ByteBuf byteBuf = (ByteBuf) msg;
        System.out.println(ctx.channel().remoteAddress() +":"+ byteBuf.toString(CharsetUtil.UTF_8));
        channelGroup.forEach(channel -> {
            if (channel!= ctx.channel()) {
                channel.writeAndFlush("【客户端】"+ctx.channel().remoteAddress() +"发送了消息:"+ byteBuf.toString(CharsetUtil.UTF_8));
            }else {
                channel.writeAndFlush("【自己】发送了消息:"+ byteBuf.toString(CharsetUtil.UTF_8));

            }
        });
    }*/

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        //获取到当前 channel
        Channel channel = ctx.channel();
        //这时我们遍历 channelGroup, 根据不同的情况， 回送不同的消息
        channelGroup.forEach(ch -> {
            if (channel != ch) { //不是当前的 channel,转发消息
                ch.writeAndFlush("[ 客户端 ]" + channel.remoteAddress() + " 发送了消息：" + msg + "\n");
            } else {//回显自己发送的消息给自己
                ch.writeAndFlush("[ 自己 ]发送了消息：" + msg + "\n");
            }
        });
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
//         ByteBuf msg = Unpooled.copiedBuffer("HelloClient",CharsetUtil.UTF_8);
//         ctx.writeAndFlush(msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        Channel channel = ctx.channel();
        //将该客户加入聊天的信息推送给其它在线的客户端
        //该方法会将 channelGroup 中所有的 channel 遍历，并发送消息
        channelGroup.writeAndFlush("[ 客户端 ]" + channel.remoteAddress() + " 上线了 " + simpleDateFormat.format(new
                java.util.Date())+ "\n");
        //将当前 channel 加入到 channelGroup
        channelGroup.add(channel);
        System.out.println(ctx.channel().remoteAddress() + " 上线了"+ "\n");
//
//        channelGroup.forEach(channel -> {
//            if (channel!= ctx.channel()) {
//                channel.writeAndFlush("【客户端】"+ctx.channel().remoteAddress() +"上线了");
//            }
//        });
//        channelGroup.add(ctx.channel());
////        channelGroup.writeAndFlush("【客户端】:"+ctx.channel().remoteAddress()+"上线了 "+simpleDateFormat.format(new Date()) );
//        System.out.println("【客户端】:"+ctx.channel().remoteAddress()+"上线了"+ "\n");

    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Channel channel = ctx.channel();
        //将客户离开信息推送给当前在线的客户
        channelGroup.writeAndFlush("[ 客户端 ]" + channel.remoteAddress() + " 下线了"+ "\n");
        System.out.println(ctx.channel().remoteAddress() + " 下线了"+ "\n");
        System.out.println("channelGroup size=" + channelGroup.size());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("error......."+cause.getMessage());
        ctx.close();
    }
}
