package com.zhangal.hurding.netty.pipeline;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

/**
 * @ClassName InPipeline
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/8 10:30
 **/
public class InPipeline {

    static class SimpleInboundHandlerA extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            System.out.println("回调:SimpleHandlerA.channelRead()");
            super.channelRead(ctx, msg);
        }
    }


    static class SimpleInboundHandlerB extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
           System.out.println("回调:SimpleHandlerB.channelRead()");
//            super.channelRead(ctx, msg);
            ctx.fireChannelRead(msg);
            //不调用super.channelRead（） 数据流会被截断
        }
    }

    static class SimpleInboundHandlerC extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            System.out.println("回调:SimpleHandlerC.channelRead()");
            super.channelRead(ctx, msg);
        }
    }

    @Test
    public void testPipelineInBound() {
        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(new SimpleInboundHandlerA());
                pipeline.addLast(new SimpleInboundHandlerB());
                pipeline.addLast(new SimpleInboundHandlerC());
            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        ByteBuf buffer = Unpooled.buffer();
        buffer.writeByte(1);
        embeddedChannel.writeInbound(buffer);
        embeddedChannel.flush();
        embeddedChannel.close();

    }
}
