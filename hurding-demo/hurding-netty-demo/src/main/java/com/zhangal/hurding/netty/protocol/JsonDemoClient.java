package com.zhangal.hurding.netty.protocol;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

public class JsonDemoClient {

    private String content = "勤奋是进步的重要的途径！";


   public void sendMsg(String host ,int port) {

       EventLoopGroup eventLoopGroup = new NioEventLoopGroup(1);
       Bootstrap b = new Bootstrap();
       b.channel(NioSocketChannel.class);
       b.group(eventLoopGroup);
       b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);

       try {

           b.handler(new ChannelInitializer<SocketChannel>() {
               @Override
               protected void initChannel(SocketChannel socketChannel) throws Exception {
                   ChannelPipeline pipeline = socketChannel.pipeline();
                   pipeline.addLast(new LengthFieldPrepender(4));
                   pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));
               }
           });
           ChannelFuture future = b.connect(host, port);
           future.addListener(new ChannelFutureListener() {
               @Override
               public void operationComplete(ChannelFuture channelFuture) throws Exception {
                  if (channelFuture.isSuccess()) {
                      System.out.println("连接服务器端成功。");
                  }else {
                      System.out.println("连接服务端失败。");
                  }
               }
           });
           future.sync();
           Channel channel = future.channel();

           for(int i=0;i<10000;i++) {
               JsonMsg jsonMsg = buildJsonMsg(i + 1, i + 1 + "——>" + content);
               future.channel().writeAndFlush(jsonMsg.toJsonStr());
               System.out.println("发送报文："+jsonMsg.toJsonStr());
           }
           channel.flush();

           ChannelFuture sync = channel.closeFuture().sync();

       } catch (InterruptedException e) {
           e.printStackTrace();
       }finally {
            eventLoopGroup.shutdownGracefully();
       }


   }


   public JsonMsg buildJsonMsg(Integer id,String content) {
       JsonMsg jsonMsg = new JsonMsg();
       jsonMsg.setContent(content);
       jsonMsg.setId(id);
       return jsonMsg;
   }


    public static void main(String[] args) {
       String host = "127.0.0.1";
       int port = 8081;
       new JsonDemoClient().sendMsg(host,port);
    }
}
