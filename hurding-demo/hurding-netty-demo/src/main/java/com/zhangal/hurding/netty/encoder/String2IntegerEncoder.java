package com.zhangal.hurding.netty.encoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

public class String2IntegerEncoder extends MessageToMessageEncoder<String> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, String msg, List<Object> list) throws Exception {
        char[] chars = toString().toCharArray();
        for (int i = 0; i < chars.length; i++) {
           char c =  chars[i];
            if (c>=48 && c <= 57) {
                list.add(new Integer(c));
            }
        }

    }
}
