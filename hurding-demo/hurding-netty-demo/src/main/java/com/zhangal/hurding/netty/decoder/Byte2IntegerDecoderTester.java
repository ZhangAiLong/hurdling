package com.zhangal.hurding.netty.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

/**
 * @ClassName Byte2IntegerDecoderTester
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/9 10:11
 **/
public class Byte2IntegerDecoderTester {


    @Test
    public void testByteToIntegerDecoder() {

        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(new Byte2IntegerDecoder());
                pipeline.addLast(new IntegerProcessHandler());

            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 10; i++) {
            ByteBuf byteBuf = Unpooled.buffer();
            byteBuf.writeInt(i);
            embeddedChannel.writeInbound(byteBuf);
        }

        embeddedChannel.flush();
        embeddedChannel.close().addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                if (channelFuture.isSuccess()) {
                    System.out.println("监听端口成功");
                }else {
                    System.out.println("监听端口失败");
                }
            }
        });
    }


    @Test
    public void testByte2IntgerReplayDecoder() {
        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(new Byte2IntegerReplayDecoder());
                pipeline.addLast(new IntegerProcessHandler());

            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 10; i++) {
            ByteBuf byteBuf = Unpooled.buffer();
            byteBuf.writeInt(i);
            embeddedChannel.writeInbound(byteBuf);
        }

        embeddedChannel.flush();
        embeddedChannel.close().addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                if (channelFuture.isSuccess()) {
                    System.out.println("监听端口成功");
                }else {
                    System.out.println("监听端口失败");
                }
            }
        });
    }
}
