package com.zhangal.hurding.netty.basic;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

public class NettyDiscardHandler extends ChannelInboundHandlerAdapter {

    public void channelRead(ChannelHandlerContext ctx,Object msg) {
        try {
            ByteBuf in = (ByteBuf)msg;
            System.out.println("接到消息，丢弃如下：");
            while (in.isReadable()) {
                System.out.println((char)in.readByte());
            }
            System.out.println(); //换行
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

}
