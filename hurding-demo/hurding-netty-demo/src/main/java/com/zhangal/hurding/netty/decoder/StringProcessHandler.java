package com.zhangal.hurding.netty.decoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @ClassName StringProcessHandler
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/9 21:07
 **/
public class StringProcessHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String content = (String)msg;
        System.out.println("StringProcessHandler msg: "+content);
        super.channelRead(ctx, msg);

    }
}
