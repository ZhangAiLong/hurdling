package com.zhangal.hurding.netty.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

/**
 * @ClassName Byte2IntegerReplayDecoderTester
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/9 20:34
 **/
public class Byte2IntegerReplayDecoderTester {


    @Test
    public void test() throws InterruptedException {
        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(new IntegerAddDecoder());
                pipeline.addLast(new IntegerProcessHandler());
            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);

        for (int i = 0; i < 20; i++) {
            ByteBuf byteBuf = Unpooled.buffer();
            int num = i+1;
            System.out.println(num);
            byteBuf.writeInt(num);
            embeddedChannel.writeInbound(byteBuf);
        }
        embeddedChannel.flush();

    }

}
