package com.zhangal.hurding.netty.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import org.junit.Test;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName NettyOpenBoxDecoder
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/10 11:08
 **/
public class NettyOpenBoxDecoder {

    static String content= "勤奋学习是进步的重要途径！";


    @Test
    public void testLengthFieldBasedFrameDecoder1() {
        LengthFieldBasedFrameDecoder frameDecoder = new LengthFieldBasedFrameDecoder(1024,0,4,0,4);

        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(frameDecoder);
                pipeline.addLast(new StringDecoder(Charset.forName("UTF-8")));
                pipeline.addLast(new StringProcessHandler());
            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 100 ; i++) {
            ByteBuf byteBuf = Unpooled.buffer();
            String msg = i+"次发送-->" + content;
            byte[] bytes = msg.getBytes(Charset.forName("UTF-8"));
            System.out.println(bytes.length);
            byteBuf.writeInt(bytes.length);
            byteBuf.writeBytes(bytes);
            embeddedChannel.writeInbound(byteBuf);
            embeddedChannel.flush();
        }


    }


    @Test
    public void testLengthFieldBasedFrameDecoder2() throws InterruptedException {
        LengthFieldBasedFrameDecoder frameDecoder = new LengthFieldBasedFrameDecoder(1024,0,4,2,6);

        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(frameDecoder);
                pipeline.addLast(new StringDecoder(Charset.forName("UTF-8")));
                pipeline.addLast(new StringProcessHandler());
            }
        };
        int length = "一".getBytes(StandardCharsets.UTF_8).length;
        System.out.println("汉字一占用"+length+"个字节");
        length = "11".getBytes(StandardCharsets.UTF_8).length;
        System.out.println("阿拉伯数字1占用"+length+"个字节");
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 100 ; i++) {
            ByteBuf byteBuf = Unpooled.buffer();
            String msg = i+"次发送-->" + content;
            byte[] bytes = msg.getBytes(Charset.forName("UTF-8"));
//            System.out.println(bytes.length);
            byteBuf.writeInt(bytes.length);
            byteBuf.writeBytes("一".getBytes(Charset.forName("GBK")));
            byteBuf.writeBytes(bytes);
            embeddedChannel.writeInbound(byteBuf);
            embeddedChannel.flush();
        }

        Thread.sleep(1000);
    }


    @Test
    public void testLengthFieldBasedFrameDecoder() {

        LengthFieldBasedFrameDecoder frameDecoder = new LengthFieldBasedFrameDecoder(1024,2,4,4,10);

        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(frameDecoder);
                pipeline.addLast(new StringDecoder(Charset.forName("UTF-8")));
                pipeline.addLast(new StringProcessHandler());
            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 100 ; i++) {
            ByteBuf byteBuf = Unpooled.buffer();
            String msg = i+"次发送-->" + content;
            byte[] bytes = msg.getBytes(Charset.forName("UTF-8"));
            byteBuf.writeChar(1010);
            byteBuf.writeInt(bytes.length);
            byteBuf.writeInt(10);
            byteBuf.writeBytes(bytes);
            embeddedChannel.writeInbound(byteBuf);
            embeddedChannel.flush();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
