package com.zhangal.hurding.netty.protocol;

import com.zhangal.hurding.common.utils.JsonUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class JsonServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String str = (String)msg;
        JsonMsg jsonMsg = JsonUtil.jsonToPojo(str, JsonMsg.class);
        System.out.println("接收到消息："+jsonMsg);
        super.channelRead(ctx, msg);


    }
}
