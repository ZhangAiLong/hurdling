package com.zhangal.hurding.netty.bytebuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import org.junit.Test;

/**
 * @ClassName WriteReadTest
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/8 14:25
 **/
public class WriteReadTest {


    private void readByteBuf(ByteBuf byteBuf) {
        if (byteBuf!=null) {
            while (byteBuf.isReadable()) {
              System.out.println("取一个字节："+byteBuf.readByte());
            }
        }
    }

    private void getByteBuf(ByteBuf byteBuf) {
        if (byteBuf != null) {
            for (int i = 0; i < byteBuf.readableBytes(); i++) {
                System.out.println("读一个字节："+byteBuf.getByte(i));
            }
        }
    }


    public void print(String msg,ByteBuf byteBuf) {
       System.out.println("after ================"+msg+"=========================");
        System.out.println("1.0 isReadable: "+byteBuf.isReadable());
        System.out.println("1.1 readerIndex: "+byteBuf.readerIndex());
        System.out.println("1.2 readableBytes: "+byteBuf.readableBytes());
        System.out.println("2.0 isWritable: "+byteBuf.isWritable());
        System.out.println("2.1 writerIndex: "+byteBuf.writerIndex());
        System.out.println("2.2 writableBytes: "+byteBuf.writableBytes());
        System.out.println("3.0 capacity: "+byteBuf.capacity());
        System.out.println("3.1 maxCapacity: "+byteBuf.maxCapacity());
        System.out.println("3.1 maxWritableBytes: "+byteBuf.maxWritableBytes());

    }


    @Test
    public  void testWriteRead() {
        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer(9,100);
        print("动作：分配 ByteBuf(9, 100)",byteBuf);
        byteBuf.writeBytes(new byte[]{1,2,3,4,5,6,7,8,9,10,11});
        print("动作：写入4个字节 {1,2,3,4}",byteBuf);
        System.out.println("start ===========:get==================");
        getByteBuf(byteBuf);
        System.out.println("start ===========:read1==================");
        readByteBuf(byteBuf);
        System.out.println("start ===========:read2==================");
        readByteBuf(byteBuf);
        System.out.println("start ===========:get2==================");
        getByteBuf(byteBuf);
        print("动作：读完ByteBuf",byteBuf);
    }
}
