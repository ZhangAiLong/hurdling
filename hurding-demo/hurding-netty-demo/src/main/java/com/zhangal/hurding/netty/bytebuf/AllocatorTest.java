package com.zhangal.hurding.netty.bytebuf;

import io.netty.buffer.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

/**
 * @ClassName AllocatorTest
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/8 15:12
 **/
public class AllocatorTest {

    @Test
    public void testAllocator() {

        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer(9,1000);

        byteBuf = ByteBufAllocator.DEFAULT.buffer();

        byteBuf = Unpooled.buffer(100,3000);

        ByteBuf buffer = UnpooledByteBufAllocator.DEFAULT.buffer(100, 30000);


        byteBuf = UnpooledByteBufAllocator.DEFAULT.heapBuffer();


        byteBuf = PooledByteBufAllocator.DEFAULT.buffer(100,1000);

        //池化创建bytebuf
        PooledByteBufAllocator.DEFAULT.directBuffer();

    }


    public static void printByteBuf(String action, ByteBuf b) {
        System.out.println(" ===========" + action + "============");
        //true 表缓冲区为 Java 堆内存（组合缓冲例外）
        //false 表缓冲区为操作系统管理的内存（组合缓冲例外）
        System.out.println("b.hasArray: " + b.hasArray());
        //输出内存分配器
        System.out.println("b.ByteBufAllocator: " + b.alloc());
    }

    static class AllocDemoHandler extends ChannelInboundHandlerAdapter {
        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg)
                throws Exception
        {
            printByteBuf("入站的 ByteBuf", (ByteBuf) msg);
            ByteBuf buf = ctx.alloc().buffer();
            buf.writeInt(100);
            //向模拟通道写一个出站包，模拟数据出站，需要刷新通道才能获取到输出
            ctx.channel().writeAndFlush(buf);
        }
    }

    @Test
    public void testByteBufAlloc() {
        ChannelInitializer i = new ChannelInitializer<EmbeddedChannel>(){

            @Override
            protected void initChannel(EmbeddedChannel embeddedChannel) throws Exception {
                ChannelPipeline pipeline = embeddedChannel.pipeline();
                pipeline.addLast(new AllocDemoHandler());
            }
        };

        EmbeddedChannel channel = new EmbeddedChannel(i);
        channel.config().setAllocator(PooledByteBufAllocator.DEFAULT);
        ByteBuf buf = Unpooled.buffer();
        buf.writeInt(1);
        channel.writeInbound(buf);
        ByteBuf outBuf = (ByteBuf)channel.readOutbound();
        printByteBuf("出站的 ByteBuf", (ByteBuf) outBuf);

        channel.flush();
        channel.close();

    }
}
