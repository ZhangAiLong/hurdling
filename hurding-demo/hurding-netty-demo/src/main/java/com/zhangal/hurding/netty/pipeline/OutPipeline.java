package com.zhangal.hurding.netty.pipeline;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

import java.nio.ByteBuffer;

/**
 * @ClassName OutPipeline
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/8 10:48
 **/
public class OutPipeline {


    static class SimpleChannelOutInboundA extends ChannelOutboundHandlerAdapter {

        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.println("出站被回调：SimpleChannelOutInboundA ");
           super.write(ctx, msg, promise);
        }
    }

    static class SimpleChannelOutInboundB extends ChannelOutboundHandlerAdapter {

        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.println("出站被回调：SimpleChannelOutInboundB ");
            super.write(ctx, msg, promise);
        }
    }


    static class SimpleChannelOutInboundC extends ChannelOutboundHandlerAdapter {

        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.println("出站被回调：SimpleChannelOutInboundC");
            super.write(ctx, msg, promise);
        }
    }


    @Test
    public void testPipelineOutBound() {
        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(new SimpleChannelOutInboundA());
                pipeline.addLast(new SimpleChannelOutInboundB());
                pipeline.addLast(new SimpleChannelOutInboundC());
            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        ByteBuf buffer = Unpooled.buffer();
        buffer.writeInt(10);
        embeddedChannel.writeOutbound(buffer);
        embeddedChannel.flush();
        embeddedChannel.close();


    }
}
