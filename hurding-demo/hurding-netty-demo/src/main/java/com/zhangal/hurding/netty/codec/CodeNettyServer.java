package com.zhangal.hurding.netty.codec;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class CodeNettyServer {
   public static int port = 9020;
   public void startServer() throws InterruptedException {
       ServerBootstrap bootstrap = new ServerBootstrap();
       EventLoopGroup workEventLoop = new NioEventLoopGroup(2);
       EventLoopGroup boosEventLoop = new NioEventLoopGroup(1);

       bootstrap.group(boosEventLoop,workEventLoop);
       bootstrap.channel(NioServerSocketChannel.class).option(ChannelOption.SO_BACKLOG,1024).option(ChannelOption.SO_KEEPALIVE,true);
       bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
           @Override
           protected void initChannel(SocketChannel socketChannel) throws Exception {
               ChannelPipeline pipeline = socketChannel.pipeline();
               pipeline.addLast(new LongToByteEncoder());
               pipeline.addLast(new ByteToLongDecoder());
               pipeline.addLast(new CodeNettyServerHandler());
           }
       });

       ChannelFuture future = bootstrap.bind(port).sync();
       future.channel().closeFuture().sync();

       boosEventLoop.shutdownGracefully();
       workEventLoop.shutdownGracefully();

   }


    public static void main(String[] args) throws InterruptedException {
          new CodeNettyServer().startServer();;
    }
}
