package com.zhangal.hurding.netty.decoder;

import com.zhangal.hurding.common.utils.RandomUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * @ClassName StringReplayDecoderTester
 * @Description 字符串的分包解码器的实践案例
 * @Author HQJN
 * @Date 2022/3/9 21:08
 **/
public class StringReplayDecoderTester {

    static String content= "勤奋是进步的唯一有效途径！";

    @Test
    public void testStringReplayDecoder() {

        ChannelInitializer channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel channel) throws Exception {
                ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast(new StringReplayDecoder());
                pipeline.addLast(new StringProcessHandler());
            }
        };

        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);

        //待发送字符串 content 的字节数组
        byte[] bytes =content.getBytes(Charset.forName("utf-8"));
        //循环发送 100 轮，每一轮可以理解为发送一个 Head-Content 报文

        for (int i = 0; i < 20; i++) {
            int random = RandomUtil.randInMod(3);
            ByteBuf byteBuf = Unpooled.buffer();
            byteBuf.writeInt(random * bytes.length);
            for (int j = 0; j < random; j++) {
                byteBuf.writeBytes(bytes);
            }
            embeddedChannel.writeInbound(byteBuf);
        }
        embeddedChannel.flush();

    }

}
