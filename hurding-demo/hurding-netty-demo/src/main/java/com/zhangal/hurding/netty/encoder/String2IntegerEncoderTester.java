package com.zhangal.hurding.netty.encoder;

import com.zhangal.hurding.netty.decoder.Integer2StringDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

public class String2IntegerEncoderTester {


    @Test
    public void  testerString2IntegerEncoder() {

        ChannelInitializer i = new ChannelInitializer<EmbeddedChannel>() {
            protected void initChannel(EmbeddedChannel ch) {
                ch.pipeline().addLast(new Integer2ByteEncoder());
                ch.pipeline().addLast(new String2IntegerEncoder());
            }
        };
//        EmbeddedChannel channel = new EmbeddedChannel(i);
//        for (int j = 0; j < 100; j++) {
//            channel.write(j); // 向着通道写入整数
//        }
        EmbeddedChannel channel = new EmbeddedChannel(i);
        for (int j = 0; j < 10; j++) {
            String s = "i am " + j;
            channel.write(s); //向着通道写入含有数字的字符串
        }
        channel.flush();
        //取得通道的出站数据包
        ByteBuf buf = (ByteBuf) channel.readOutbound();
        while (null != buf) {
            System.out.println("o = " + buf.readInt()); //打印数字
            buf = (ByteBuf) channel.readOutbound(); // 读取数字
        }
    }
}
