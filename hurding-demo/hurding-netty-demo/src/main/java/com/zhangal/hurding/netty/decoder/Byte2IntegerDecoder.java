package com.zhangal.hurding.netty.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @ClassName Byte2IntegerDecoder
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/9 10:11
 **/
public class Byte2IntegerDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        if (byteBuf!=null) {
            if (byteBuf.readableBytes()>4) {
                int readInt = byteBuf.readInt();
                list.add(readInt);
            }
        }
    }
}
