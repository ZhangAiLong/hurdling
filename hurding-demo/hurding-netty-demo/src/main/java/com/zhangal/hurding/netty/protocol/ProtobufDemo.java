package com.zhangal.hurding.netty.protocol;


import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ProtobufDemo {


    public static MsgProtos.Msg buildMsg() {
        MsgProtos.Msg.Builder newBuilder = MsgProtos.Msg.newBuilder();
        newBuilder.setContent("勤奋是进步的重要途径！");
        newBuilder.setId(100);
        return newBuilder.build();

    }

    @Test
    public void serAndDesr1() throws IOException {
        MsgProtos.Msg msg = buildMsg();
        byte[] bytes = msg.toByteArray();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(bytes);

        byte[] data = outputStream.toByteArray();
        MsgProtos.Msg msg1 = MsgProtos.Msg.parseFrom(data);
//        System.out.println(msg1);
        System.out.println("id: "+msg1.getId());
        System.out.println("contnet: "+msg1.getContent());

    }

    @Test
    public void serAndDesr2() throws IOException {
        MsgProtos.Msg msg = buildMsg();
        byte[] bytes = msg.toByteArray();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(bytes);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(outputStream.toByteArray());
        MsgProtos.Msg msg1 = MsgProtos.Msg.parseFrom(byteArrayInputStream);
        System.out.println("id: "+msg1.getId());
        System.out.println("contnet: "+msg1.getContent());
    }

    @Test
    public void serAndDesr3() throws IOException {
        MsgProtos.Msg msg = buildMsg();
        byte[] bytes = msg.toByteArray();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        msg.writeDelimitedTo(outputStream);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(outputStream.toByteArray());
        MsgProtos.Msg msg1 = MsgProtos.Msg.parseDelimitedFrom(byteArrayInputStream);
        System.out.println("id: "+msg1.getId());
        System.out.println("contnet: "+msg1.getContent());
    }
}
