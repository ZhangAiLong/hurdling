package com.zhangal.hurding.netty.decoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @ClassName IntegerProcessHandler
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/9 10:13
 **/
public class IntegerProcessHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        Integer num = (Integer) msg;
        System.out.println("======num="+num+"===========");
    }
}
