package com.zhangal.hurding.netty.protocol;

import com.zhangal.hurding.common.utils.JsonUtil;

public class JsonMsg {

    private Integer id;
    private String content;


    public String toJsonStr() {
        return JsonUtil.pojoToJson(this);
    }

    public JsonMsg parseFromJson(String jsonStr) {
        return JsonUtil.jsonToPojo(jsonStr,JsonMsg.class);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
