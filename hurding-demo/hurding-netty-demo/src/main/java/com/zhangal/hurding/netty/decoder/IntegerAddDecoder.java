package com.zhangal.hurding.netty.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * @ClassName IntegerAddDecoder
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/9 20:26
 **/
public class IntegerAddDecoder extends ReplayingDecoder<IntegerAddDecoder.PHASE> {

    //自定义的状态枚举值，代表两个阶段
    enum PHASE
    {
        PHASE_1,//第一个阶段，则仅仅提取第一个整数，完成后进入第二个阶段
        PHASE_2 //第二个整数，提取后还需要结算相加的结果，并且输出结果
    }

    private Integer first;
    private Integer second;


    public IntegerAddDecoder() {
        super(PHASE.PHASE_1);
    }


    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> list) throws Exception {

        switch (state()) {

            case PHASE_1:
                first = in.readInt();
                checkpoint(PHASE.PHASE_2);
                break;
            case PHASE_2:
                second = in.readInt();
                Integer sum = first + second;
                list.add(sum);
                checkpoint(PHASE.PHASE_1);
                break;
            default:
                break;
        }
    }


}
