package com.zhangal.hurding.netty.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * @ClassName StringReplayDecoder
 * @Description TODO
 * @Author HQJN
 * @Date 2022/3/9 20:56
 **/
public class StringReplayDecoder  extends ReplayingDecoder<StringReplayDecoder.PHASE> {

    enum PHASE{
        PHASE_1,  //第一阶段，解码出字符串的长度
        PHASE_2;  //第二阶段，按照第一阶段的字符串长度解除字符串内容
    };
    StringReplayDecoder() {
       //构造函数中，需要初始化父类的 state 属性为 PHASE_1 阶段
        super(PHASE.PHASE_1);
    }
    private int length;
    private byte[] inBytes;
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> list) throws Exception{
        switch (state()) {
            case PHASE_1:
                 length = in.readInt();
                inBytes = new byte[length];
                checkpoint(PHASE.PHASE_2);
                break;
            case PHASE_2:
                in.readBytes(inBytes, 0, length);
                list.add((new String(inBytes, "UTF-8")));
                checkpoint(StringReplayDecoder.PHASE.PHASE_1);
                break;
            default:
                break;
        }

    }
}
