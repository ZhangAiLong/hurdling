package com.zhangal.hurding.zk.distributedLock;

public interface Lock {

    boolean lock() throws Exception;

    boolean unlock();
}
