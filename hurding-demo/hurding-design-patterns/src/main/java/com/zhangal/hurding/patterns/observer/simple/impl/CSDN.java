package com.zhangal.hurding.patterns.observer.simple.impl;

import com.zhangal.hurding.patterns.observer.simple.Blogger;
import com.zhangal.hurding.patterns.observer.simple.Fans;

import java.util.ArrayList;
import java.util.List;

public class CSDN implements Blogger {

    private List<Fans> fansList = new ArrayList<>();

    @Override
    public void addFans(Fans fans) {
        this.fansList.add(fans);
    }

    @Override
    public void removeFans(Fans fans) {
        this.fansList.remove(fans);
    }

    @Override
    public void sendMessage(String message) {
        for(Fans fans : fansList){
            fans.receiveMessage(message);
        }
    }
}
