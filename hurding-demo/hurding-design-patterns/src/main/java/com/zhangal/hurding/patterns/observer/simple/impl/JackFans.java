package com.zhangal.hurding.patterns.observer.simple.impl;

import com.zhangal.hurding.patterns.observer.simple.Fans;

public class JackFans implements Fans {
    @Override
    public void receiveMessage(String message) {
        System.out.println("Jack收到了私信："+message);
    }
}
