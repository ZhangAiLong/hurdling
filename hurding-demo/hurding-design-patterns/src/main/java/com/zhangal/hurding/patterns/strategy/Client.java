package com.zhangal.hurding.patterns.strategy;

public class Client {

    public static void main(String[] args) {

        IMemberStrategy commonMemberStrategy = new CommonMemberStrategy();
        IMemberStrategy  middleMemberStrategy = new MiddleMemberStrategy();
        IMemberStrategy  seniorMemberStrategy = new SeniorMemberStrategy();

        Context commonMemberContext = new Context(commonMemberStrategy);
        Context middleMemberContext = new Context(middleMemberStrategy);
        Context seniorMemberContext = new Context(seniorMemberStrategy);

        Double price = 300.0;
        int num = 1;

        System.out.println("普通会员的价格："+  commonMemberContext.selectedCountPrice(price,num));
        System.out.println("中级会员的价格："+ middleMemberContext.selectedCountPrice(price,num));
        System.out.println("高级会员的价格："+ seniorMemberContext.selectedCountPrice(price,num));
    }
}
