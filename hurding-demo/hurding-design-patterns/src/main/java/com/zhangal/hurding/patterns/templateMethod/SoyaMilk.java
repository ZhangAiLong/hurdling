package com.zhangal.hurding.patterns.templateMethod;

public abstract class SoyaMilk {

    public boolean isAddCondiments() {
        return true;
    }


    //制作黄豆过程:选材==》添加配料==》浸泡==》放到豆浆机打碎。
    public final void make() {
       selectSoya();
       if (isAddCondiments()) {
           addCondiments();
       }
       soak();
       beat();
    }
    //
    public void selectSoya() {
        System.out.println("选择上好的黄豆。。。。。。");
    }

    public abstract void addCondiments();

    public void soak() {
        System.out.println("食材开始浸泡。。。。。。。");
    }

    public void beat() {
        System.out.println("开始放到豆浆机打碎。。。。。");
    }
}
