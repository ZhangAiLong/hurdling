package com.zhangal.hurding.patterns.observer.jdk;

import java.util.Observable;
import java.util.Observer;

public class Client {


    public static void main(String[] args) {
        Observable csdn = new CSDN();
        Observer tomFans = new TomFans();
        Observer jackFans = new JackFans();
        csdn.addObserver(tomFans);
        csdn.addObserver(jackFans);
        csdn.notifyObservers("《关于整顿社区的通知》");
        csdn.deleteObserver(tomFans);
        csdn.notifyObservers("《关于更新CSDN社区的通知》");
    }

}
