package com.zhangal.hurding.patterns.observer.simple;

import com.zhangal.hurding.patterns.observer.simple.impl.CSDN;
import com.zhangal.hurding.patterns.observer.simple.impl.JackFans;
import com.zhangal.hurding.patterns.observer.simple.impl.TomcatFans;

public class Client {


    public static void main(String[] args) {
        Blogger blogger = new CSDN();
        Fans jackFans = new JackFans();
        Fans tomFans = new TomcatFans();
        blogger.addFans(jackFans);
        blogger.addFans(tomFans);
        blogger.sendMessage("CSDN发布了《关于社区整顿的通知》");
        blogger.removeFans(jackFans);
        blogger.sendMessage("CSDN发布了《关于博客发布调整的通知》");

    }
}
