package com.zhangal.hurding.patterns.strategy;

public class CommonMemberStrategy implements IMemberStrategy{
    @Override
    public Double countPrice(Double price, int num) {
        return price * num;
    }
}
