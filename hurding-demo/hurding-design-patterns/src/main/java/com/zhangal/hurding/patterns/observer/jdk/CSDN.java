package com.zhangal.hurding.patterns.observer.jdk;

import java.util.Observable;

public class CSDN extends Observable {

    @Override
    public void notifyObservers(Object arg) {
        //修改状态为可以群发
        setChanged();
        //调用父类的notifyObservers 群发消息
        super.notifyObservers(arg);
    }

}
