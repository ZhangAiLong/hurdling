package com.zhangal.hurding.patterns.strategy;

/**
 * 会员计算价格策略接口
 */
public interface IMemberStrategy {

    /**
     *
     * @param price
     * @param num
     * @return
     */
    public Double countPrice(Double price,int num);


}
