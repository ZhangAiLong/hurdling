package com.zhangal.hurding.patterns.templateMethod;

public class PeanutSoyaMilk extends SoyaMilk{
    @Override
    public void addCondiments() {
        System.out.println("添加美味的花生酱。。。");
    }

    @Override
    public boolean isAddCondiments() {
        return false;
    }
}

