package com.zhangal.hurding.patterns.strategy;
/**
 * 负责和具体的策略类交互
 * 这样的话，具体的算法和直接的客户端调用分离了，使得算法可以独立于客户端独立的变化。
 */

// 上下文类/环境类
public class Context {

    private IMemberStrategy memberStrategy;

    public Context(IMemberStrategy memberStrategy) {
        this.memberStrategy = memberStrategy;
    }


    public Double selectedCountPrice(Double price,int num) {
        return memberStrategy.countPrice(price,num);
    }
}
