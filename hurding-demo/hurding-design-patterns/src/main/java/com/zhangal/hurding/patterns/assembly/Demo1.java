package com.zhangal.hurding.patterns.assembly;

interface Car {
    void drive();
}

class Engine implements Car {
    @Override
    public void drive() {
        System.out.println("发动机启动，汽车开始行驶。");
    }
}

class Tires implements Car {
    @Override
    public void drive() {
        System.out.println("轮胎转动，汽车开始行驶。");
    }
}

interface CarFactory {
    Car getCar();
}

class EngineFactory implements CarFactory {
    @Override
    public Car getCar() {
        return new Engine();
    }
}

class TiresFactory implements CarFactory {
    @Override
    public Car getCar() {
        return new Tires();
    }
}

public class Demo1 {
    public static void main(String[] args) {
        // 创建一个发动机工厂
        CarFactory engineFactory = new EngineFactory();

        // 使用发动机工厂组装汽车
        Car car1 = engineFactory.getCar();

        // 创建一个轮胎工厂
        CarFactory tiresFactory = new TiresFactory();

        // 使用轮胎工厂组装汽车
        Car car2 = tiresFactory.getCar();

        // 行驶汽车
        car1.drive();
        car2.drive();
    }
}
