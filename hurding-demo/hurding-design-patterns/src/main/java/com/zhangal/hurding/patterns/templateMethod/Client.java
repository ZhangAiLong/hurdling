package com.zhangal.hurding.patterns.templateMethod;

public class Client {


//    模板方法模式的基本思想是：算法骨架只存在特定父类中。当需要变更算法骨架，则只需要修改一处。
//    实现了代码复用的最大化。
//    提供了很大的灵活性，父类的算法骨架保证了整体的统一性，子类的可以实现部分细节，保证了细节的灵活性。
//    缺点：每一个不同的实现都需要一个子类，可能导致类过多。
//    一般模板方法都会加上final关键字，防止子类重写模板方法。
//    使用场景：若某个过程包含一些列相同的步骤，且只在个别步骤的实现上有所差别，通常考虑使用模板方法来实现


    public static void main(String[] args) {
        SoyaMilk peanutSoyaMilk = new PeanutSoyaMilk();
        // 制作花生豆浆
        peanutSoyaMilk.make();

        System.out.println("************************************************************************");


        SoyaMilk sugarSoyaMilk = new SugarSoyaMilk();
        // 制作糖果豆浆
        sugarSoyaMilk.make();

    }
}
