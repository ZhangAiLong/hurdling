package com.zhangal.hurding.patterns.observer.simple;

public interface Blogger {

    /**
     * 新增粉丝
     */
    void addFans(Fans fans);

    /**
     * 移除粉丝
     */
    void removeFans(Fans fans);

    /**
     * 通知粉丝
     */
    void sendMessage(String message);
}