package com.zhangal.hurding.patterns.templateMethod;

public class SugarSoyaMilk extends SoyaMilk{
    @Override
    public void addCondiments() {
        System.out.println("添加美味的糖果...");
    }

    @Override
    public boolean isAddCondiments() {
        return true;
    }
}
