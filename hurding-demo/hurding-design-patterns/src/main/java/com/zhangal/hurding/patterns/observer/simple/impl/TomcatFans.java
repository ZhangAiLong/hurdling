package com.zhangal.hurding.patterns.observer.simple.impl;

import com.zhangal.hurding.patterns.observer.simple.Fans;

public class TomcatFans implements Fans {
    @Override
    public void receiveMessage(String message) {
        System.out.println("Tomcat收到了私信："+message);
    }
}
