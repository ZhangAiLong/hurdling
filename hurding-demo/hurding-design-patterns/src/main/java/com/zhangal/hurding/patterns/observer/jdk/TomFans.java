package com.zhangal.hurding.patterns.observer.jdk;

import java.util.Observable;
import java.util.Observer;

public class TomFans implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Tom收到了私信："+arg);
    }
}
