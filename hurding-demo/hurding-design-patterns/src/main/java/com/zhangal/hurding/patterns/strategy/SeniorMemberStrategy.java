package com.zhangal.hurding.patterns.strategy;

public class SeniorMemberStrategy implements IMemberStrategy{
    @Override
    public Double countPrice(Double price, int num) {
        return price*num * ( 1- 0.2);
    }
}
