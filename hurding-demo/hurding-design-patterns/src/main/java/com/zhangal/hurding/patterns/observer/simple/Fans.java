package com.zhangal.hurding.patterns.observer.simple;

public interface Fans {

    void receiveMessage(String message);
}
