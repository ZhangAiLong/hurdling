package com.zhangal.hurding.spring.ioc.test;

import com.zhangal.hurding.spring.ioc.dao.UserDao;
import com.zhangal.hurding.spring.ioc.dto.UserDto;
import com.zhangal.hurding.spring.ioc.entity.Role;
import com.zhangal.hurding.spring.ioc.entity.User;
import com.zhangal.hurding.spring.ioc.service.ABeanService;
import com.zhangal.hurding.spring.ioc.service.BBeanService;
import com.zhangal.hurding.spring.ioc.service.RoleService;
import com.zhangal.hurding.spring.ioc.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextBuildUserTest {


    public static void main(String[] args) {
        //以前的方式创建对象
//        User user = new User();
//        System.out.println(user);

        //现在使用Spring框架的容器来创建对象
        //第一步:创建Spring容器
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        ABeanService aBeanService = ac.getBean("aBeanService", ABeanService.class);
        BBeanService bBeanService = ac.getBean("bBeanService", BBeanService.class);
        aBeanService.sayHello();
        aBeanService.sayHello2();
        bBeanService.sayHello();
        /*第二步:从ApplicationContext容器获取Bean对象
         * 1.构造器获取对象
         *         语法格式：
         *             类型名 变量名 = 容器对象.getBean("对象标识符",组件类型);
         *         举例：
         *             User user = ac.getBean("user", User.class);
         * 2.静态工厂获取对象
         *         语法格式：
         *             类型名 变量名 = 容器对象.getBean("静态工厂标识符",组件类型);
         *         举例：
         *             User user = ac.getBean("staticFactory", User.class);
         * */
//        User user = ac.getBean("dynamicCreateUser", User.class);
//        System.out.println(user);

//        UserDao userDao1 = ac.getBean("userDao", UserDao.class);
//        System.out.println(userDao1);
//        UserDao userDao2 = ac.getBean("userDao", UserDao.class);
//        System.out.println(userDao2);

//        UserService userService = ac.getBean("userService", UserService.class);
//        UserDto user1 = userService.getUser(99);
//        System.out.println(user1);
//
//        UserService userServiceCreate = ac.getBean("userServiceCreate", UserService.class);
//        UserDto user2 = userServiceCreate.getUser(199);
//        System.out.println(user2);
//
//        UserService staticCreateUserService = ac.getBean("staticCreateUserService", UserService.class);
//        UserDto user299 = staticCreateUserService.getUser(299);
//        System.out.println(user299);
//
//        RoleService roleService = ac.getBean("roleService", RoleService.class);
//        Role role = roleService.getRole(299);
//        System.out.println(role);
    }
}
