package com.zhangal.hurding.spring.ioc.service;

import com.zhangal.hurding.spring.ioc.dto.UserDto;

public interface IUserService {

    /**
     * 根据id获取用户
     * @param id
     * @return
     */
    public UserDto getUser(Integer id);

    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     * @return
     */
    public UserDto login(String username,String password);


    /**
     * 创建新用户
     * @param username
     * @param password
     * @param confimPassowrd
     * @return
     */
    public UserDto createUser(String username,String password,String confimPassowrd);


    /**
     * 更新用户
     * @param userDto
     */
    public void updateUser(UserDto userDto);

}
