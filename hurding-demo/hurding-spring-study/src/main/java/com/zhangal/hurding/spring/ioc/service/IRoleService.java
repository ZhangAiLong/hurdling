package com.zhangal.hurding.spring.ioc.service;

import com.zhangal.hurding.spring.ioc.entity.Role;

public interface IRoleService {

    public Role getRole(Integer id);
}
