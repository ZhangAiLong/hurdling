package com.zhangal.hurding.spring.aop.service;

public interface AccountService {

    void transfer(String fromCardNo,String toCardNo,int money) throws Exception;

}
