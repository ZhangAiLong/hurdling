package com.zhangal.hurding.spring.ioc.service;

import com.zhangal.hurding.spring.ioc.dao.UserDao;
import com.zhangal.hurding.spring.ioc.dto.UserDto;
import com.zhangal.hurding.spring.ioc.entity.User;

public class UserService implements IUserService{

    private UserDao userDao;

    public void initService() {
        System.out.println("......UserService.init............");
    }
    public void destoryService() {
        System.out.println(".......UserService.destory............");
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDto getUser(Integer id) {
        User user = userDao.getUser(id);
        return this.convertDto(user);
    }

    @Override
    public UserDto login(String username, String password) {
        System.out.println("user service.login()");
        return new UserDto(9999,username,password);
    }

    @Override
    public UserDto createUser(String username, String password, String confimPassowrd) {
        System.out.println("userservice.createUser()");
        return null;
    }

    @Override
    public void updateUser(UserDto userDto) {
        System.out.println("userservice.updateUser()");

    }

    private UserDto convertDto(User user) {
        if (user != null) {
           return new UserDto(user.getId(),user.getUsername(),user.getPassword());
        }
        return null;
    }

    private User convertUser(UserDto userDto) {
        if (userDto != null) {
            return new User(userDto.getId(),userDto.getUsername(),userDto.getPassword());
        }
        return null;
    }
}
