package com.zhangal.hurding.spring.aop.dao;


import com.zhangal.hurding.spring.aop.entity.Account;

/**
 * @author 应癫
 */
public interface AccountDao {

    Account queryAccountByCardNo(String cardNo) throws Exception;

    int updateAccountByCardNo(Account account) throws Exception;
}
