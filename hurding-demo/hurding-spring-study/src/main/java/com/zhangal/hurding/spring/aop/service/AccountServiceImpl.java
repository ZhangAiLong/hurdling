package com.zhangal.hurding.spring.aop.service;


import com.zhangal.hurding.spring.aop.dao.AccountDao;
import com.zhangal.hurding.spring.aop.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService{


    @Autowired
    private AccountDao accountDao;


    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {
        Account fromAccount = accountDao.queryAccountByCardNo(fromCardNo);
        Account toAccount = accountDao.queryAccountByCardNo(toCardNo);
        fromAccount.setMoney(fromAccount.getMoney()-money);
        toAccount.setMoney(toAccount.getMoney()+money);
        accountDao.updateAccountByCardNo(fromAccount);
        accountDao.updateAccountByCardNo(toAccount);
    }
}
