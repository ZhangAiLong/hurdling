package com.zhangal.hurding.spring.aop.controller;

import com.zhangal.hurding.spring.aop.entity.Result;
import com.zhangal.hurding.spring.aop.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "account")
public class AccountController {



    @Autowired
    private AccountService accountService;

//    public void setAccountService(AccountService accountService) {
//        this.accountService = accountService;
//    }

    @RequestMapping(value = "/transfer",method = RequestMethod.GET)
    public Result transfe(@RequestParam("fromCardNo") String fromCardNo,@RequestParam("toCardNo") String toCardNo,
                                  @RequestParam("money") String money) throws Exception {
        Result result = new Result();;
        try {
            accountService.transfer(fromCardNo,toCardNo,Integer.valueOf(money));
            result.setStatus("200");
            result.setMessage("转账成功");
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setStatus("201");
            result.setMessage(ex.getMessage());
        }

         return result;
    }


}
