package com.zhangal.hurding.spring.ioc.service;

public class ABeanService {


    private BBeanService bBeanService;


    public void setbBeanService(BBeanService bBeanService) {
        this.bBeanService = bBeanService;
    }
    public void sayHello() {
        System.out.println("=========aBeanService.sayHello============");
    }

    public void sayHello2() {
        bBeanService.sayHello();
        System.out.println("=========aBeanService.sayHello2============");

    }
}
