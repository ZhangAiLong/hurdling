package com.zhangal.hurding.spring.ioc.factory;

import com.zhangal.hurding.spring.ioc.dao.UserDao;
import com.zhangal.hurding.spring.ioc.service.UserService;

public class UserServiceFactory {

    public UserService createUserService(){
        UserService userService = new UserService();
        userService.setUserDao(new UserDao());
        return userService;
    }

    public static UserService staticCreateUserService() {
        UserService userService = new UserService();
        userService.setUserDao(new UserDao());
        return userService;
    }
}
