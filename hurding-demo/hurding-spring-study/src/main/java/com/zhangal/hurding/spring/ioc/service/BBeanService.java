package com.zhangal.hurding.spring.ioc.service;

public class BBeanService {


    public void setaBeanService(ABeanService aBeanService) {
        this.aBeanService = aBeanService;
    }

    private ABeanService aBeanService;

    public void sayHello() {
        aBeanService.sayHello();
        System.out.println("=========bBeanService.sayHello============");
    }
}
