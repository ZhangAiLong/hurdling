package com.zhangal.hurding.mq.kafka;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Collections;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class KafkaProducerSerialIzerTest implements Runnable{

    private final KafkaProducer<String, String> producer;

    private final String topic;

    private final String bookerHost = "zookeeper-host:9092";


    public KafkaProducerSerialIzerTest(String topicName) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "zookeeper-host:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("key.serializer", StringSerializer.class.getName());
        props.put("value.serializer", StringSerializer.class.getName());
        this.producer = new KafkaProducer<String, String>(props);
        this.topic = topicName;
        autoCreateTopic(this.topic);
    }


    private void autoCreateTopic(String topicName) {
        Properties properties = new Properties();
        properties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, bookerHost);
        // 创建管理者
        AdminClient adminClient = AdminClient.create(properties);
        try {
            // 创建topic前，可以先检查topic是否存在，如果已经存在，则不用再创建了
            Set<String> topics = adminClient.listTopics().names().get();
            if (topics.contains(topicName)) {
                return;
            }
            System.out.println("======newTopic========================");

            // 创建topic
            NewTopic newTopic = new NewTopic(topicName, 3, (short) 1);
            CreateTopicsResult result = adminClient.createTopics(Collections.singletonList(newTopic));
            result.all().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            adminClient.close();
        }

    }


    @Override
    public void run() {
        int messageNo = 1;
        try {
            for(;;) {
                String messageStr="你好，这是第"+messageNo+"条数据";
                producer.send(new ProducerRecord<String, String>(topic, "Message", messageStr));
                //生产了100条就打印
                if(messageNo%100==0){
                    System.out.println("发送的信息:" + messageStr);
                }
                //生产1000条就退出
                if(messageNo%1000000==0){
                    System.out.println("成功发送了"+messageNo+"条");
                    break;
                }
                messageNo++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            producer.close();
        }
    }
    public static void main(String[] args) {
        KafkaProducerSerialIzerTest test = new KafkaProducerSerialIzerTest("t-name1");
        Thread thread = new Thread(test);
        thread.start();

    }
}
