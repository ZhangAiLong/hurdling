package com.zhangal.hurding.mq.kafka;

import org.apache.kafka.clients.admin.*;
import org.apache.kafka.common.KafkaFuture;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

public class KafkaAdminClient {
   private static final AdminClient adminClient = KafkaAdminClient.getAdminClient(KafkaAdminClient.bootstrapServers,"4000");
    public static final String bootstrapServers = "10.1.1.13:9092,10.1.1.23:9093,10.1.1.23:9092";


    public static AdminClient getAdminClient(String address, String requestTimeoutMS) {
        Properties properties = new Properties();
        properties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, address);
        properties.setProperty(AdminClientConfig.REQUEST_TIMEOUT_MS_CONFIG, requestTimeoutMS);
        return AdminClient.create(properties);
    }


    public void createTopic(String topicName,int numPartitions,short replicationFactor) throws InterruptedException, ExecutionException {
        CountDownLatch latch = new CountDownLatch(1);
        Set<String> topics = adminClient.listTopics().names().get();
        if (topics.contains(topicName)) {
            return;
        }
        Set<NewTopic> singleton = Collections.singleton(new NewTopic(topicName, numPartitions, replicationFactor));
        CreateTopicsResult topicsResult = adminClient.createTopics(singleton);
        topicsResult.all().get();
//        Map<String, KafkaFuture<Void>> values = topics.values();
//        values.forEach((name_,future) ->{
//            future.whenComplete((a,throwable) ->{
//               if ( throwable != null) {
//                   System.out.println(throwable.getMessage());
//               }
//               System.out.println(name_);
//                latch.countDown();
//
//            });
//        });

//        latch.await();
    }

    public void describeTopic(String name) {
        Map<String, KafkaFuture<TopicDescription>> values = adminClient.describeTopics(Collections.singleton(name)).values();
        for (String name__ : values.keySet()) {
            values.get(name__).whenComplete((describe,throwable)-> {
               if (throwable != null) {
                   System.out.println(throwable.getMessage());
               }
               System.out.println(name__);
               System.out.println(describe);
            });
        }

    }


    public void addNumPartitions(String name,int numParitions) throws ExecutionException, InterruptedException {
        Map<String, NewPartitions> partitionsMap = new HashMap<>();
        NewPartitions newPartitions = NewPartitions.increaseTo(numParitions);
        partitionsMap.put(name,newPartitions);
        adminClient.createPartitions(partitionsMap).all().get();
    }


    public static void main(String[] args) throws InterruptedException, ExecutionException {
        KafkaAdminClient kafkaAdminClient = new KafkaAdminClient();
        String new_topic_user = "new-topic-user";
        kafkaAdminClient.createTopic(new_topic_user,1,(short) 1);
        kafkaAdminClient.describeTopic(new_topic_user);
        //增加topic
        kafkaAdminClient.addNumPartitions(new_topic_user,4);
//

        Thread.sleep(100000);
        kafkaAdminClient.describeTopic(new_topic_user);
    }


}
