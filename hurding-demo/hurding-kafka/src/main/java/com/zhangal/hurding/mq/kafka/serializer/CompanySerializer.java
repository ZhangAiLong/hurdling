package com.zhangal.hurding.mq.kafka.serializer;

import com.zhangal.hurding.mq.kafka.vo.Company;
import org.apache.kafka.common.serialization.Serializer;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class CompanySerializer implements Serializer<Company> {


    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, Company data) {
        if (data == null) {
            return null;
        }
        byte[] name, address;

        try {
            if (data.getName()!=null) {
                name = data.getName().getBytes(StandardCharsets.UTF_8);
            }else {
                name = new byte[0];
            }
            if (data.getAddress() !=null) {

            }
        } catch (Exception ex) {

        }
        return new byte[0];
    }

    @Override
    public void close() {

    }
}
