package com.zhangal.hurding.mq.kafka;

import com.zhangal.hurding.mq.kafka.constants.KafkaConstants;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Arrays;
import java.util.Properties;

public class KafkaConsumerPartitions {

    public static final String bootstrapServers = "10.1.1.13:9092,10.1.1.23:9093,10.1.1.23:9092";
    private  KafkaConsumer<String, String> consumer =null;
    private ConsumerRecords<String, String> msgList;
    private final String topic = KafkaConstants.NEW_TOPIC_USER;
    private static final String GROUPID = "groupB";
   public Properties initConfig() {

       Properties props = new Properties();
       props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
       props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUPID);
       props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
       props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
       props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
       props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
       props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
       props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
       props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
       props.put(ConsumerConfig.CLIENT_ID_CONFIG,"client.id.demo");
//       this.consumer = new KafkaConsumer<String, String>(props);
//       this.consumer.subscribe(Arrays.asList(topic));
//       TopicPartition topicPartition0 = new TopicPartition(this.topic,0);
//       this.consumer.assign(Arrays.asList(topicPartition0));
       return props;
   }

   public void doConsume(Properties properties) {
       KafkaConsumer<String,String> consumer= new KafkaConsumer<String,String>(properties) ;
       TopicPartition topicPartition0 = new TopicPartition(this.topic,0);
       //订阅主题1分区数据
       consumer.assign(Arrays.asList(topicPartition0));
       int messageNo = 1;
       try {
           for (;;) {
               msgList = consumer.poll(1000);
               if(null!=msgList&&msgList.count()>0){
                   for (ConsumerRecord<String, String> record : msgList) {
                       System.out.println(messageNo+"==topicPartition="+record.partition()+"=====receive: key = " + record.key() + ", value = " + record.value()+" offset==="+record.offset());
                       messageNo++;
                   }
               }else{
                   Thread.sleep(1000);
               }
           }
       } catch (InterruptedException e) {
           e.printStackTrace();
       } finally {
           consumer.close();
       }

   }


    public static void main(String args[]) {

        KafkaConsumerPartitions test1 = new KafkaConsumerPartitions();
        Properties properties = test1.initConfig();
        test1.doConsume(properties);
//       this.consumer = new KafkaConsumer<String, String>(props);
//       this.consumer.subscribe(Arrays.asList(topic));
//       TopicPartition topicPartition0 = new TopicPartition(this.topic,0);
//       this.consumer.assign(Arrays.asList(topicPartition0));
}
}
