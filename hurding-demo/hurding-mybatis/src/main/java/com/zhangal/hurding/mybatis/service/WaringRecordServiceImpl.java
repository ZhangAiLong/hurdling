package com.zhangal.hurding.mybatis.service;

import com.zhangal.hurding.mybatis.entity.WarningRecord;
import com.zhangal.hurding.mybatis.mapper.WarningRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WaringRecordServiceImpl implements IWaringRecordService {

    @Autowired
    WarningRecordMapper warningRecordMapper;

    @Override
    public void deleteRecord(Integer warningType) {

    }

    @Override
    public Integer deleteRecordInbatch(Integer warningType, Integer limitNumber) {
        int deleteTotalNum = 0;
        try {
            while (true) {
                List<WarningRecord> warningRecords = warningRecordMapper.selectByType(warningType, 1);
                if (warningRecords==null || warningRecords.size()==0) {
                     break;
                }
                int i = warningRecordMapper.deleteByWarningTypeWithLimit(warningType, limitNumber);
                System.out.println("deleteNum:"+i);
                deleteTotalNum += i;
//                Thread.sleep(10);
            }

        }catch (Exception ex) {
            ex.printStackTrace();
        }
        return deleteTotalNum;
    }



}
