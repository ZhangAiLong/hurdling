package com.zhangal.hurding.mybatis.service;

public interface IWaringRecordService {

    public void deleteRecord(Integer warningType);

    public Integer deleteRecordInbatch(Integer warningType,Integer limitNumber);
}
