package com.zhagnal.hurding.mybatis;

import com.zhangal.hurding.mybatis.MybatisApplication;
import com.zhangal.hurding.mybatis.entity.WarningRecord;
import com.zhangal.hurding.mybatis.mapper.WarningRecordMapper;
import com.zhangal.hurding.mybatis.service.IWaringRecordService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;

@SpringBootTest(classes = MybatisApplication.class)
public class WarningRecordTest {


    @Autowired
    WarningRecordMapper warningRecordMapper;

    @Autowired
    IWaringRecordService waringRecordService;

    @Test
    public void test() {
        List<WarningRecord> query = warningRecordMapper.query(new HashMap<>());
        if (query!=null) {
            for (int i = 0; i < query.size() ; i++) {
                WarningRecord warningRecord = query.get(i);
                System.out.println(warningRecord.getId()+","+warningRecord.getProjectId());
            }
        }

    }

    @Test
    public void tesDeleteRecord() {
        Integer warningType = 10;
        Integer limitnumber = 100000;
        Integer recordTotalNum = waringRecordService.deleteRecordInbatch(warningType, limitnumber);
        System.out.println("-----recordTotalNum---"+recordTotalNum);
    }

}
