package com.zhangal.demo.jedis.hyperLogLog;

import com.zhangal.demo.jedis.JedisLink;
import redis.clients.jedis.Jedis;

/**
 * @ClassName PfTest
 * @Description TODO
 * @Author HQJN
 * @Date 2022/1/28 9:10
 **/
public class PfTest {

    public static void main(String[] args) {
        Jedis jedis = JedisLink.getInstance();
        for (int i=0; i<1000; i++) {
            jedis.pfadd("uv","user"+i);
            long total = jedis.pfcount("uv");
            if (total != i+1) {
                System.out.printf("%d %d\n",total,i+1);
                break;
            }
        }
        jedis.close();
    }
}
