package com.zhangal.demo.jedis.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;




public class User implements Serializable {

        private Integer id;

        private String name;

        private String phoneNumber;

        private Date birthday;

        private Boolean isDeleted;

        private String password;

        private Date createTime;

        private Date updateTime;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getPhoneNumber() {
                return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
        }

        public Date getBirthday() {
                return birthday;
        }

        public void setBirthday(Date birthday) {
                this.birthday = birthday;
        }

        public Boolean getDeleted() {
                return isDeleted;
        }

        public void setDeleted(Boolean deleted) {
                isDeleted = deleted;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public Date getCreateTime() {
                return createTime;
        }

        public void setCreateTime(Date createTime) {
                this.createTime = createTime;
        }

        public Date getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(Date updateTime) {
                this.updateTime = updateTime;
        }


}
