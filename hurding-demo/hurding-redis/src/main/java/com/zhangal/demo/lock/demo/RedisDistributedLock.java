package com.zhangal.demo.lock.demo;

import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class RedisDistributedLock implements Lock {

    //上下文，保存当前锁的持有人Id
    private ThreadLocal<String> lockContext = new ThreadLocal<String>();

    //默认锁的超时时间
    private  long time  = 100;

    //可重入性
    private Thread ownerThread;

    public RedisDistributedLock() {}

    @Override
    public void lock() {
       while (!tryLock()) {
           try {
               Thread.sleep(1000);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @SneakyThrows
    @Override
    public boolean tryLock() {
        return tryLock(time,TimeUnit.MILLISECONDS);
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        String id = UUID.randomUUID().toString();  //每个所的持有人分配一个唯一id
        Thread t = Thread.currentThread();
        Jedis jedis = new Jedis("10.1.1.14",6379);
        //只有锁不存之地额时候加锁并设置锁的有效时间
//        String rs = jedis.set("lock", id, "NX", "PX", unit.toMillis(time));
        String rs = jedis.set("lock",id,new SetParams().nx().ex(time));
        System.out.println("rs="+rs);
        if ("OK".equals(rs)) {
            lockContext.set(id);
            setOwnerThread(t);
            return true;
        }else if (ownerThread == t) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void unlock() {
        try {
            String script = null;
            Jedis jedis = new Jedis("10.1.1.14",6379);
            InputStream  resourceAsStream = new FileInputStream("D:\\3-workspace\\hurdling\\hurding-demo\\hurding-redis\\src\\main\\java\\com\\zhangal\\demo\\lock\\demo\\Redis.lua");
            script = inputStream2String(resourceAsStream);
            System.out.println("script="+script);
            if (lockContext.get()==null) {
                return;
            }
            //删除锁
            jedis.eval(script, Arrays.asList("lock"),Arrays.asList(lockContext.get()));
            lockContext.remove();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Condition newCondition() {
        return null;
    }

    public String inputStream2String(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = -1;
        while ((i=is.read())!= -1) {
            baos.write(i);
        }
        return baos.toString();

    }

    private void setOwnerThread(Thread t) {
        this.ownerThread = t;
    }

    public static void main(String[] args) {
        RedisDistributedLock lock = new RedisDistributedLock();
        lock.lock();
        lock.unlock();
    }
}
