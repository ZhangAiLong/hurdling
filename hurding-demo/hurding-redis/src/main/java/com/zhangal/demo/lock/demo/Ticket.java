package com.zhangal.demo.lock.demo;


import java.util.concurrent.TimeUnit;

/**
 * 卖票
 */
public class Ticket {

    /**
     * 初始库存量
     */
    Integer ticketNum = 8;

    public void reduce(Integer num) {
        //判断库存是否能够
        String name = Thread.currentThread().getName();
        if (ticketNum-num>=0) {
            try {
                TimeUnit.MILLISECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ticketNum -= num;
            System.out.println(name+"成功卖出的"+num+"张，剩余"+ticketNum+"张票");
        }else {
            System.err.println(name+"没有卖出的"+num+"张，剩余"+ticketNum+"张票");
        }

    }

    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        for (int i=0;i<10;i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ticket.reduce(1);
                }
            });
            thread.setName("用户"+i+1);
            thread.start();
        }
    }
}
