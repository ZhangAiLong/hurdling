package com.zhangal.demo.jedis.queue.pubsub;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.zhangal.demo.jedis.JedisLink;
import redis.clients.jedis.Jedis;

import java.lang.reflect.Type;
import java.util.Set;
import java.util.UUID;

/**
 * @ClassName RedisDelayingQueue
 * @Description TODO
 * @Author HQJN
 * @Date 2022/1/27 16:56
 **/
public class RedisDelayingQueue <T> {


   static class TaskItem<T> {
       public String id;

       public T msg;
   }


   private Type TaskType = (Type) new TypeReference<TaskItem<T>>() {}.getType();

   private Jedis jedis;
   private String queueKey;

   public RedisDelayingQueue(Jedis jedis,String queueKey) {
       this.jedis = jedis;
       this.queueKey = queueKey;
   }

   public void delay(T msg) {
       TaskItem<T> task = new TaskItem<>();
       //分配唯一的 uuid
       task.id = UUID.randomUUID().toString();
       task.msg = msg;
       String taskJson = JSON.toJSONString(task);
       // 保存到延时队列，5s 后再试
       long l = System.currentTimeMillis() + 5000;
       System.out.println(".....l="+l+"........");
       jedis.zadd(queueKey,l, taskJson);
   }

   public void loop() {
       while (!Thread.interrupted()) {
           // 只取一条
           Set<String> values = jedis.zrangeByScore(queueKey, 0, System.currentTimeMillis(), 0, 1);
           System.out.println("*************values*"+values.isEmpty()+"***");
           if (values.isEmpty()) {
               try {
                   Thread.sleep(500);
               } catch (InterruptedException e) {
                   break;
               }
               continue;
           }
           String s = values.iterator().next();
           if (jedis.zrem(queueKey,s) > 0) {
               TaskItem<T> task = JSON.parseObject(s, TaskType);
               this.handleMsg(task.msg);
           }
       }
   }


   public void handleMsg(T msg) {
       System.out.println("ts:"+System.currentTimeMillis()+", msg:"+msg);
   }


    public static void main(String[] args) {
        Jedis jedis = JedisLink.getInstance();
        RedisDelayingQueue<String> queue = new RedisDelayingQueue<>(jedis,"delay-queue");
        Thread producer = new Thread() {
            public void run() {
                for (int i = 0; i < 10; i++) {
                    queue.delay("jjjsdfsdfsfsdgsddsdsdf"+i);
                }
            }
        };

        Thread consumer = new Thread() {
            public void run() {
                queue.loop();
            }
        };
        producer.start();;
        System.out.println("=====producer.start()=======");
        consumer.start();
        System.out.println("=====consumer.start()=======");

        try {
            System.out.println("=====producer.join()==start====");
            producer.join();
            System.out.println("=====producer.join()==end====");
            System.out.println("=====Thread.sleep(6000)==start====");
            Thread.sleep(6000);
            System.out.println("=====Thread.sleep(6000)==end====");
            System.out.println("=====consumer.interrupt()==start====");
            consumer.interrupt();
            System.out.println("=====consumer.interrupt()==end====");
            System.out.println("=====consumer.join()==start====");
            consumer.join();
            System.out.println("=====consumer.join()==end====");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
