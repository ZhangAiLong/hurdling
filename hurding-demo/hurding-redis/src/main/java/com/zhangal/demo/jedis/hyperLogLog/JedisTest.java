package com.zhangal.demo.jedis.hyperLogLog;

import com.zhangal.demo.jedis.JedisLink;
import redis.clients.jedis.Jedis;

/**
 * @ClassName PfTest
 * @Description TODO
 * @Author HQJN
 * @Date 2022/1/28 9:10
 **/
public class JedisTest {

    public static void main(String[] args) {
        Jedis jedis = JedisLink.getInstance();
        for (int i=0; i<10000; i++) {
            jedis.pfadd("uv2","user"+i);

        }
        long total = jedis.pfcount("uv2");
        System.out.printf("%d %d\n",10000,total);
        jedis.close();
    }
}
