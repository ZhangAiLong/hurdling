package com.zhangal.demo.jedis.queue;

import com.zhangal.demo.jedis.JedisLink;
import redis.clients.jedis.Jedis;

public class JedisQueueSimple {

    public static void main(String[] args) {
        Jedis jedis = JedisLink.instance;
        String queue = "queue";
        consumer(queue,jedis);
    }


    public static Object consumer(String queue, Jedis jedis) {
        String msg = null;
        while (true) {
            msg = jedis.rpop(queue);
            //没有消息，休眠2s
            if (msg == null) {
                try {
                    Thread.sleep(2000);
                    continue;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //处理消息
            handle(msg);
        }

    }


    public static void handle(String msg) {
        System.out.println(msg);
    }
}
