package com.zhangal.demo.lock.demo;

import java.util.concurrent.CountDownLatch;

public class Movice2 {

    private final CountDownLatch latch = new CountDownLatch(1);

    private Integer babyTickets = 20;

    private Integer spiderTickets = 100;

    public   void showBabyTickets() throws InterruptedException {
        synchronized(babyTickets) {
            System.out.println("魔童娜扎的剩余票数为："+babyTickets);
            latch.await();;
        }

    }

    public  void showSpiderTickets() throws InterruptedException {
        synchronized(spiderTickets) {
            System.out.println("蜘蛛侠的剩余票数为："+spiderTickets);

        }

    }

    public static void main(String[] args) {
        Movice2 movice = new Movice2();
        new Thread(()->{
            try {
                movice.showBabyTickets();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"用户A").start();

        new Thread(() -> {
            try {
                movice.showSpiderTickets();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ;
        },"用户B").start();
    }
}
