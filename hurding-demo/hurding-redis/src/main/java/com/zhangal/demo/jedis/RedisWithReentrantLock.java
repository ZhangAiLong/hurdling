package com.zhangal.demo.jedis;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

import java.util.HashMap;
import java.util.Map;

public class RedisWithReentrantLock {

    private ThreadLocal<Map<String,Integer>> localers = new ThreadLocal<>();
    private Jedis jedis;

    public RedisWithReentrantLock(Jedis jedis) {
        this.jedis = jedis;
    }

    private boolean _lock(String key) {
        SetParams setParams = new SetParams();
        setParams.nx().ex(50l);
        return jedis.set(key, "555", setParams)!=null;
    }

    private void _unlock(String key) {
        jedis.del(key);
    }

    private Map<String,Integer> currentLockers() {
        Map<String, Integer> refs = localers.get();
        if (refs != null) {
            return refs;
        }
        localers.set(new HashMap<>());
        return localers.get();
    }

    public boolean lock(String key) {
        Map<String, Integer> refs = currentLockers();
        Integer refCnt = refs.get(key);
        if (refCnt!=null) {
            refs.put(key,refCnt+1);
            return true;
        }
        boolean lock = this._lock(key);
        if (!lock) {
            return false;
        }

        refs.put(key,1);
        return true;
    }

    public boolean unlock(String key) {
        Map<String, Integer> refs = currentLockers();
        Integer refCnt = refs.get(key);
        if (refCnt == null) {
            return false;
        }
        refCnt -= 1;
        if (refCnt > 0) {
           refs.put(key,refCnt);
        } else {
            refs.remove(key);
            this._unlock(key);
        }
        return true;
    }



    public static void main(String[] args) {
        HostAndPort hp = new HostAndPort("10.1.1.13",6379);
        Jedis jedis = new Jedis(hp);
        jedis.select(1);
        RedisWithReentrantLock redis = new RedisWithReentrantLock(jedis);
        System.out.println(redis.lock("codehole"));
        System.out.println(redis.lock("codehole"));
        System.out.println(redis.lock("codehole"));
//        System.out.println(redis.unlock("codehole"));
//        System.out.println(redis.unlock("codehole"));
//        System.out.println(redis.unlock("codehole"));
    }
}
