package com.zhangal.demo.lock.zk;

public interface Lock {

    boolean lock() throws Exception;

    boolean unlock();
}
