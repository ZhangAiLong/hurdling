package com.zhangal.demo.jedis.queue.pubsub;

import redis.clients.jedis.JedisPubSub;


public class RedisSubPubListener extends JedisPubSub {


    public void onMessage(String channel, String message) {
        //TODO:接收订阅频道消息后，业务处理逻辑
        System.out.println(channel + "=" + message);
    }
    /**
    　　* @description: TODO
    　　* @param  * @param null
    　　* @return {@link }
    　　* @throws
    　　* @author
    　　* @date 2022/1/27 15:18
    　　*/
    public void onPMessage(String pattern, String channel, String message) {
        System.out.println(pattern + "=" + channel + "=" + message);
    }
    // 初始化订阅时候的处理
    public void onSubscribe(String channel, int subscribedChannels) {

    }
    /**
     * @description: TODO
     * @param {@link null} 
     * @return {@link } 
     * @throws
     * @author HQJN
     * @date 2022/1/27 15:25
     */
    public void onUnsubscribe(String channel, int subscribedChannels) {
    }

    // 取消按表达式的方式订阅时候的处理
    public void onPUnsubscribe(String pattern, int subscribedChannels) {
    }

    /**
    　　* @description: TODO
    　　* @param ${tags} 
    　　* @return ${return_type} 
    　　* @throws
    　　* @author zhangal
    　　* @date 2022/1/27 15:07
    　　*/
    public void onPSubscribe(String pattern, int subscribedChannels) {
    }

}
