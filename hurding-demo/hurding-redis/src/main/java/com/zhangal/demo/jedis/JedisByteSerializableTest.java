package com.zhangal.demo.jedis;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhangal.demo.jedis.entity.User;
import com.zhangal.demo.name.ChineseName;
import org.apache.commons.lang.SerializationException;
import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JedisByteSerializableTest {

    public static void main(String[] args) {

        HostAndPort hp = new HostAndPort("10.1.1.14",6379);
        Jedis jedis = new Jedis(hp);
        jedis.select(1);
        List<User> userList = createTestData(10);
        byte[] serialize = serialize(userList);


    }

    /**
     * 创建创建测试数据
     * @param size
     * @return
     */
    public static List<User> createTestData(Integer size) {
        List<User> userList = new ArrayList<>();
        User user = null;
        for (int i=0;i<size;i++) {
            user = new User();
            user.setId((i+1));
            user.setName(ChineseName.createChineseName());
            user.setPhoneNumber("134262512342");
            user.setPassword("password1");
            user.setDeleted(false);
            user.setCreateTime(new Date());
            user.setUpdateTime(new Date());
            user.setBirthday(new Date());
            userList.add(user);

        }
        return userList;
    }


    private static ObjectMapper objectMapper = new ObjectMapper();
    private static TypeReference<List<User>> usereference =
            new TypeReference<List<User>>() {
            };

    public static  <T> T deserialize(byte[] source, Class<T> type) throws SerializationException {
//        Assert.notNull(type, "Deserialization type must not be null! Pleaes provide Object.class to make use of Jackson2 default typing.");
        if (source!=null) {
            return null;
        } else {
            try {
                return objectMapper.readValue(source, type);
            } catch (Exception var4) {
                throw new SerializationException("Could not read JSON: " + var4.getMessage(), var4);
            }
        }
    }

    public static byte[] serialize(Object source) throws SerializationException {
        if (source == null) {
            return null;
        } else {
            try {
                return objectMapper.writeValueAsBytes(source);
            } catch (JsonProcessingException var3) {
                throw new SerializationException("Could not write JSON: " + var3.getMessage(), var3);
            }
        }
    }
}
