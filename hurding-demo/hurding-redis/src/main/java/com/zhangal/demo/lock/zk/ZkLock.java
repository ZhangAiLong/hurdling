package com.zhangal.demo.lock.zk;

import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class ZkLock implements  Lock{

    private static final String ZK_PATH = "/test/lock";
    private static final String LOCK_PREFIX = ZK_PATH + "/";
    private static final long WAIT_TIME = 1000;

    //zk客户端
    CuratorFramework client = null;

    private String locked_short_path = null;
    private String locked_path = null;
    private String prior_path = null;

    final AtomicInteger lockCount = new AtomicInteger(0);
    private Thread thread;


    @Override
    public boolean lock() throws Exception {
        return false;
    }

    @Override
    public boolean unlock() {
        return false;
    }
}
