package com.zhangal.demo.jedis;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;

public class JedisLink {


    public  static Jedis instance;

    static{

        HostAndPort hp = new HostAndPort("10.1.1.14",6379);
        Jedis jedis = new Jedis(hp);
        jedis.select(1);
        instance = jedis;
    }
    public static Jedis getInstance() {
        return instance;
    }

}
