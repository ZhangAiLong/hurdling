package com.zhangal.demo.jedis.bitmap;

import com.zhangal.demo.jedis.JedisLink;
import com.zhangal.hurding.common.utils.DateHelper;
import redis.clients.jedis.Jedis;

import java.util.Date;

public class BitmapUserSignDemo {


    public static void main(String[] args) {

        Jedis jedis = JedisLink.instance;
        BitmapUserSignDemo bitmapUserSignDemo = new BitmapUserSignDemo();
        Integer userId=112;
        Date date = new Date();
//        for (int i = 0; i < 100000; i++) {
//            boolean sign = bitmapUserSignDemo.isSign(jedis, userId+i, date);
//            if (!sign) {
//                bitmapUserSignDemo.sign(jedis, userId+i, date);
//            }
//        }

        Long signNumber = bitmapUserSignDemo.signNumber(jedis, userId, date);
        System.out.println("userId="+userId+" 签到的次数："+signNumber);
        jedis.close();
    }


    public void sign(Jedis jedis,Integer userId,Date signDate) {
        String keyPrefix ="user:sign:";
        Integer dateInt = Integer.valueOf(DateHelper.formatDate(signDate,DateHelper.FORMAT_DATE_YYYYMMDD));
        jedis.setbit(keyPrefix+userId.toString(),dateInt,true);
        Boolean getbit = jedis.getbit(keyPrefix + userId.toString(), dateInt);
        System.out.println(keyPrefix+userId.toString()+" offset="+dateInt+" 的value="+getbit);
    }

    public boolean isSign(Jedis jedis,Integer userId,Date signDate) {
        String keyPrefix ="user:sign:";
        Integer dateInt = Integer.valueOf(DateHelper.formatDate(signDate,DateHelper.FORMAT_DATE_YYYYMMDD));
        Boolean getbit = jedis.getbit(keyPrefix + userId.toString(), dateInt);
        System.out.println(keyPrefix+userId.toString()+" offset="+dateInt+" 的value="+getbit);
        return getbit;
    }

    public Long signNumber(Jedis jedis,Integer userId,Date signDate) {
        String keyPrefix ="user:sign:";
        Integer dateInt = Integer.valueOf(DateHelper.formatDate(signDate,DateHelper.FORMAT_DATE_YYYYMMDD));
        Long number = jedis.bitcount(keyPrefix + userId.toString());
        return number;
    }

}
