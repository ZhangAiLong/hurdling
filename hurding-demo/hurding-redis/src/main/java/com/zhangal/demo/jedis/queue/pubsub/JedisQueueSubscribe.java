package com.zhangal.demo.jedis.queue.pubsub;

import com.zhangal.demo.jedis.JedisLink;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.util.List;

/**
 * zhangal
 */
public class JedisQueueSubscribe {
    public static void main(String[] args) {
        String channelName = "channelA";
        JedisQueueSubscribe.subscribe(channelName,JedisLink.instance);
    }

    //消费者必须先订阅队列，生产者才能发布消息，否则消息会丢失。
    public static Object subscribe(String queue, Jedis jedis) {
        jedis.subscribe(new RedisSubPubListener(),queue);
        return null;
    }


    public static void handle(String queueName,String msg) {
        System.out.println("queueName:"+queueName+", msg="+msg);
    }



}
