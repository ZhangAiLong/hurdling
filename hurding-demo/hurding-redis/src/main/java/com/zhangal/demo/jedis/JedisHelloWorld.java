package com.zhangal.demo.jedis;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;

public class JedisHelloWorld {

    public static void main(String[] args) {
        HostAndPort hp = new HostAndPort("10.1.1.14",6379);
        Jedis jedis = new Jedis(hp);
        jedis.select(1);
        jedis.set("name","HelloWorld");
        String name = jedis.get("name");
        System.out.println(name);

    }
}
