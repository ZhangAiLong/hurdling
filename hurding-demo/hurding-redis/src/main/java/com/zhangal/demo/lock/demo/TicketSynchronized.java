package com.zhangal.demo.lock.demo;


import java.util.concurrent.TimeUnit;

/**
 * 卖票
 */
public class TicketSynchronized {

    /**
     * 初始库存量
     */
    Integer ticketNum = 8;

    public Long startTime = System.currentTimeMillis();

    public synchronized void reduce(Integer num) {
        //判断库存是否能够
        String name = Thread.currentThread().getName();
        if (ticketNum-num>=0) {
            try {
                TimeUnit.MILLISECONDS.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ticketNum -= num;
            if (ticketNum==0) {
                System.out.println("耗时"+(System.currentTimeMillis()- startTime)+"毫秒");
            }
            System.out.println(name+"成功卖出的"+num+"张，剩余"+ticketNum+"张票");
        }else {
            System.err.println(name+"没有卖出的"+num+"张，剩余"+ticketNum+"张票");
        }


    }



    public void reduceByLock(int num) {
        boolean flag = false;

        synchronized (ticketNum) {
            String name = Thread.currentThread().getName();
            if (ticketNum-num>=0) {
                try {
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ticketNum -= num;
            }
            if (ticketNum==0) {
                System.out.println("耗时"+(System.currentTimeMillis()- startTime)+"毫秒");
            }

            if (flag) {
                System.out.println(name+"成功卖出的"+num+"张，剩余"+ticketNum+"张票");
            }else {
                System.err.println(name+"没有卖出的"+num+"张，剩余"+ticketNum+"张票");
            }


        }
    }


    public static void main(String[] args) {
        TicketSynchronized ticket = new TicketSynchronized();
        for (int i=0;i<10;i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    // ticket.reduce(1);
                     ticket.reduceByLock(1);
                }
            });
            thread.setName("用户"+i+1);
            thread.start();
        }
    }
}
