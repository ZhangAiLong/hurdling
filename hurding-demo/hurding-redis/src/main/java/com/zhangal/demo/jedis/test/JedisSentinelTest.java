package com.zhangal.demo.jedis.test;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName JedisSentinelTest
 * @Description TODO
 * @Author HQJN
 * @Date 2022/2/24 21:36
 **/
public class JedisSentinelTest {


    public static void main(String[] args) {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMaxTotal(20);
        jedisPoolConfig.setMinIdle(5);

        String masterName = "mymaster";
        Set<String> sentinels = new HashSet<>();
        sentinels.add(new HostAndPort("10.1.1.23",26379).toString());
        sentinels.add(new HostAndPort("10.1.1.23",26380).toString());
        sentinels.add(new HostAndPort("10.1.1.33",26379).toString());

        JedisSentinelPool jedisSentinelPool = new JedisSentinelPool(masterName,sentinels,jedisPoolConfig,3000,null);
        Jedis jedis = null;
        int i=1;
        try {
            jedis = jedisSentinelPool.getResource();
            while (true) {

                System.out.println(jedis.set("zhangal"+i,"zhangal"));
                System.out.println(jedis.get("zhangal"+i));
                Thread.sleep(2000);
                i++;
            }

        } catch (Exception e) {
            System.out.println("error:"+e.getMessage());
        } finally {
//            if (jedis!=null) {
//                jedis.close();;
//            }
        }

    }
}
