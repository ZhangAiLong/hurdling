package com.zhangal.demo.lock.demo;

import java.util.concurrent.CountDownLatch;

public class Movice {

    private final CountDownLatch latch = new CountDownLatch(1);

    private Integer babyTickets = 20;

    private Integer spiderTickets = 100;

    public synchronized  void showBabyTickets() throws InterruptedException {
        System.out.println("魔童娜扎的剩余票数为："+babyTickets);
        latch.await();;
    }

    public synchronized void showSpiderTickets() throws InterruptedException {
        System.out.println("蜘蛛侠的剩余票数为："+spiderTickets);

    }

    public static void main(String[] args) {
        Movice movice = new Movice();
        new Thread(()->{
            try {
                movice.showBabyTickets();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"用户A").start();

        new Thread(() -> {
            try {
                movice.showSpiderTickets();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ;
        },"用户B").start();
    }
}
