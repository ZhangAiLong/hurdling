package com.zhangal.demo.jedis.test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

import java.util.List;

/**
 * @ClassName JedisSingleTest
 * @Description TODO
 * @Author HQJN
 * @Date 2022/2/24 20:34
 **/
public class JedisSingleTest {

    public static void main(String[] args) {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMaxTotal(20);
        jedisPoolConfig.setMinIdle(5);

        JedisPool jedisPool = new JedisPool(jedisPoolConfig,"10.1.1.23",6379,3000,null);
        Jedis jedis = null;
        try {


        jedis = jedisPool.getResource();

        System.out.println(jedis.set("single","zhangal"));
        System.out.println(jedis.get("single"));
        Pipeline pl = jedis.pipelined();
        for (int i=0; i<10; i++) {
            pl.incr("PipelineKey");
            pl.set("zhangal"+i, "zhangal");
        }

        List<Object> objects = pl.syncAndReturnAll();
        System.out.println(objects);
        }catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //注意这里不是关闭连接，在JedisPool模式下，Jedis会被规划给资源池
            if (jedis != null) {
                jedis.close();
            }
        }


    }
}
