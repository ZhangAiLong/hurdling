package com.zhangal.demo.jedis.queue;

import com.zhangal.demo.jedis.JedisLink;
import redis.clients.jedis.Jedis;

import java.util.List;

public class JedisQueueBlockConsumer {

    public static void main(String[] args) {
        Jedis jedis = JedisLink.instance;
        String queue = "queue";
        consumer(queue,jedis);
    }


    public static Object consumer(String queue, Jedis jedis) {
        List<String> msg = null;
        while (true) {
            //// 没消息阻塞等待，0表示不设置超时时间
            msg =  jedis.brpop(0,queue);
            if (msg ==null) {
                continue;
            }
            for (int i = 0; i <msg.size() ; i++) {
                //处理消息
                handle( msg.get(0),msg.get(1));
            }

        }

    }


    public static void handle(String queueName,String msg) {
        System.out.println("queueName:"+queueName+", msg="+msg);
    }
}
