package com.zhangal.hurdling.iodemo.NIO.bufferdemo;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class BufferPrint {

    public static void printByteBuffer(Buffer byteBuffer) {
        System.out.println("capacity: "+byteBuffer.capacity());
        System.out.println("limit: "+byteBuffer.limit());
        System.out.println("position: "+byteBuffer.position()); //表示下一个可以写入的index位置
    }
}
