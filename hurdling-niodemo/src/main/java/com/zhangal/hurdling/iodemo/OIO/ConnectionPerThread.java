package com.zhangal.hurdling.iodemo.OIO;

import com.zhangal.hurdling.iodemo.NioDemoConfig;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionPerThread  implements Runnable{

    public static void main(String[] args) throws IOException {
        ConnectionPerThread connectionPerThread = new ConnectionPerThread();
        connectionPerThread.run();
    }

    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(NioDemoConfig.SOCKET_SERVER_PORT);

            while (!Thread.interrupted()) {
                Socket accept = serverSocket.accept();
                Handler handler = new Handler(accept);
                new Thread(handler).start();
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    static class Handler implements Runnable {
        final Socket socket;

        public Handler(Socket socket) {
              this.socket = socket;
              System.out.println("连接的两个端口: "+socket.getPort()+","+ socket.getLocalPort());
        }


        public void run() {
            while (true) {
                try {
                    byte[] input = new byte[NioDemoConfig.SERVER_BUFFER_SIZE];
                    socket.getInputStream().read(input);
//                str.getBytes( "utf8" ), "iso8859-1");
                    System.out.println("收到信息: " + new String(input, "utf-8"));
                    System.out.println(" ");
                    byte[] output = input;
                    socket.getOutputStream().write(output);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
