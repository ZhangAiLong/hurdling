package com.zhangal.hurdling.iodemo.NIO.bufferdemo;

import org.junit.Test;

import java.nio.IntBuffer;

public class UserBuffer {

    static IntBuffer intBuffer = null;

    public static void allocateTest() {
        //创建一个IntBuffer实例对象
        intBuffer = IntBuffer.allocate(20);
        System.out.println("=========allocate==after====================");
        BufferPrint.printByteBuffer(intBuffer);

    }


    public static void putTest() {
        for (int i = 0; i < 5; i++) {
            intBuffer.put(i);
        }
        System.out.println("=========put==after====================");
        BufferPrint.printByteBuffer(intBuffer);

    }

    public static void flip() {
        System.out.println("=========flip==after====================");
        intBuffer.flip();
        BufferPrint.printByteBuffer(intBuffer);

    }

    public static void getTest() {
        for (int i = 0; i < 2 ; i++) {
            int j = intBuffer.get();
            System.out.println("j==="+j);
        }
        System.out.println("==========after get 2 int====================");
        BufferPrint.printByteBuffer(intBuffer);

        for (int i = 0; i < 3; i++) {
            int j = intBuffer.get();
            System.out.println("j==="+j);
        }
        System.out.println("==========after get 3 int====================");
        BufferPrint.printByteBuffer(intBuffer);
    }


    public static void rewind() {
        System.out.println("=========rewind==after====================");
        intBuffer.rewind();
        BufferPrint.printByteBuffer(intBuffer);

    }


    public static void reRead() {
        for (int i = 0; i < 5 ; i++) {

            if (i==2) {
                intBuffer.mark();
            }
            //读取元素
            int j = intBuffer.get();
            System.out.println("j==="+j);
        }
        System.out.println("==========after reRead====================");
        BufferPrint.printByteBuffer(intBuffer);
    }

    public static void reset() {
        intBuffer.reset();
        System.out.println("==========after reset====================");
        BufferPrint.printByteBuffer(intBuffer);
        for (int i = intBuffer.position(); i < intBuffer.limit() ; i++) {
            int j = intBuffer.get();
            System.out.println("j==="+j);
        }
        System.out.println("==========after reset=2===================");
        BufferPrint.printByteBuffer(intBuffer);
    }

    public static void clear() {
        intBuffer.clear();
        System.out.println("==========after clear====================");
        BufferPrint.printByteBuffer(intBuffer);

    }

    public static void main(String[] args) {
        allocateTest();
        putTest();
        flip();
        getTest();
        rewind();
//        getTest();
        reRead();
        reset();
//        flip();
//        getTest();
        clear();
    }


}
