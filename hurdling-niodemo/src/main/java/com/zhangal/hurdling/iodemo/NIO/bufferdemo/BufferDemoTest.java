package com.zhangal.hurdling.iodemo.NIO.bufferdemo;


import org.junit.Test;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class BufferDemoTest {

   @Test
    public void testByteBuffer1() {
       int capaticy = 1024;
       System.out.println("---------------allocate------------------");
       ByteBuffer byteBuffer = ByteBuffer.allocate(capaticy);
       printStr("capacity: "+byteBuffer.capacity());
       printStr("limit: "+byteBuffer.limit());
       printStr("position: "+byteBuffer.position());

       System.out.println("---------------put------------------");
       String txt = "go中国!!!!";
       byteBuffer.put(txt.getBytes());
       printByteBuffer(byteBuffer);
       System.out.println("---------------flip------------------");
       byteBuffer.flip();  //切换成读取模式
       printByteBuffer(byteBuffer);

       System.out.println("---------------get------------------");
       byte[] dist = new byte[byteBuffer.limit()];
       byteBuffer.get(dist);  //一次读取 byteBuffer.limit() 长度字节
       System.out.println(new String(dist,0,dist.length));
       printByteBuffer(byteBuffer);  //可读取的limit是12，容量是1024 可读取的位置12，因为前边12位置已经背读取走

       System.out.println("---------------rewind------------------");
       byteBuffer.rewind(); //将位置设置成0，
       printByteBuffer(byteBuffer);
       byteBuffer.get(dist);  //一次读取 byteBuffer.limit() 长度字节
       System.out.println(new String(dist,0,dist.length));
       printByteBuffer(byteBuffer);

       System.out.println("---------------clear------------------");
       byteBuffer.clear();  // 数据不会被真实的清理掉 position = 0; limit = capacity; mark = -1; return this;
       printByteBuffer(byteBuffer);
       byteBuffer.get(dist);  //一次读取 byteBuffer.limit() 长度字节
       System.out.println(new String(dist,0,dist.length));

   }

    @Test
    public void testByteBuffer2() {
       ByteBuffer buffer = ByteBuffer.allocate(1024);
        String txt = "go中国!!!!";
        buffer.put(txt.getBytes());   //将txt写入到buf缓存区
        buffer.flip();                //切换成读取模式
        byte[] dist = new byte[buffer.limit()];  //定义个字节数值,长度是buf的limit
        buffer.get(dist,0,2);       //获取【0,2)的字节写入到dist字节数组中
        System.out.println(new String(dist,0,2));   // 打印结果为go
        this.printByteBuffer(buffer); //下一个读取位置为position=2 limit= 12

        buffer.mark();   //对缓冲区设置标记  position=2

        buffer.get(dist,2,3);       //获取【2,4)的字节写入到dist字节数组中
        System.out.println(new String(dist,2,3));   // 打印结果为“中” 一个汉字3个字节
        System.out.println("position: "+buffer.position()); //表示下一个可以写入的index位置 position=5

        buffer.reset();    ///恢复到mark标记的位置 position=2
        System.out.println("reset after position: "+buffer.position()); //表示下一个可以写入的index位置 position=2

        //判断缓冲区中是否还有剩余数据
        if (buffer.hasRemaining()) {
            System.out.println(buffer.remaining());  //返回position后到limit之间的元素个数 10
        }


    }

    @Test
    public  void testByteBuffer3() {
       //分配缓冲区到直接内存中
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1024);
        System.out.println(byteBuffer.isDirect());  //true

        ByteBuffer allocate = ByteBuffer.allocate(1024);
        System.out.println(allocate.isDirect()); //false

    }


    private void printStr(String content) {
       System.out.println(content);
    }
    private void printByteBuffer(ByteBuffer byteBuffer) {
        System.out.println("capacity: "+byteBuffer.capacity());
        System.out.println("limit: "+byteBuffer.limit());
        System.out.println("position: "+byteBuffer.position()); //表示下一个可以写入的index位置
    }
}
