package com.zhangal.hurdling.iodemo.NIO.bufferdemo;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class ChannelTest {

    @Test
    public void write() throws FileNotFoundException {
       try {
            FileOutputStream fileOutputStream = new FileOutputStream("D:\\4-workspace\\data01.txt");
            FileChannel channel = fileOutputStream.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(1024);

            for (int i = 0; i < 100; i++) {
                buffer.clear();
                buffer.put(("hello,使用Buffer和channel实现写数据到文件中"+i+"\r\n").getBytes());
                buffer.flip();
                channel.write(buffer);
            }
            channel.close();
            System.out.println("写数据到文件中！");
       }catch (Exception e) {
           e.printStackTrace();
       }

    }


}
