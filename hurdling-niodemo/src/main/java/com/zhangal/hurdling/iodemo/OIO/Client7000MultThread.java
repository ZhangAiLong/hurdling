package com.zhangal.hurdling.iodemo.OIO;

import com.zhangal.hurdling.iodemo.NioDemoConfig;
import io.netty.util.HashedWheelTimer;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Client7000MultThread {

    static HashedWheelTimer timer = new HashedWheelTimer(1, TimeUnit.SECONDS,16);

    public static void main(String[] args) throws IOException {
       for (int i=0;i<70000; i++) {
            Socket socket = new Socket("localhost", NioDemoConfig.SOCKET_SERVER_PORT);
            System.out.println((i+1)+"连接的两个端口: "+socket.getPort()+","+ socket.getLocalPort());
            Handler handler = new Handler(socket, i);
            new Thread(handler).start();
        }
    }


    static class Handler implements Runnable {

       final Socket socket;

       private final int index;

       private CountDownLatch latch;

       public Handler(Socket socket,int index) {
           this.socket = socket;
           this.index = index;
       }

        public void run() {
           while (true) {
               if (socket!=null) {
                   System.out.println(index+"客户端连接服务器成功！");
               }

               try {
                   socket.getOutputStream().write(("from"+this.index+"").getBytes("utf-8"));
                   Thread.sleep(100);
               } catch (IOException e) {
                   e.printStackTrace();
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }

        }
    }
}
