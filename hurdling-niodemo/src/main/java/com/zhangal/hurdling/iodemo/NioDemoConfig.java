package com.zhangal.hurdling.iodemo;

public class NioDemoConfig {

    //oio server port
    public final static int SOCKET_SERVER_PORT = 18899;


    public final static int SERVER_BUFFER_SIZE = 1024;
}
